<?php
namespace Deployer;

require 'recipe/symfony.php';
require 'contrib/rsync.php';

// Project name
set('application', 'obw21.portfolio.syamsudin.dev');

// Project repository
set('repository', 'git@bitbucket.org:jebong/obw-cms.git');
set('branch', 'master');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false);
set('ssh_multiplexing', true);

// Shared files/dirs between deploys
add('shared_files', [
    '.env.local',
    'public/.htaccess',
]);
add('shared_dirs', [
    'var/data',
    'var/sessions'
]);

// Writable dirs by web server
add('writable_dirs', [
    'var/cache',
    'var/log',
    'var/data',
    'var/sessions'
]);
set('allow_anonymous_stats', false);

// Hosts
host('128.199.135.108')
    ->set('labels', ['stage' => 'production'])
    ->set('deploy_path', '~/app/{{ application }}')
    ->set('remote_user', 'syamsudin')
    ->set('port', 22884)
    ->set('bin/php', '/usr/local/bin/ea-php74')
    ->set('rsync_src', '/opt/atlassian/pipelines/agent/build')
    ->set('rsync_dest','{{release_path}}');
    //->addSshOption('ControlMaster', 'no')
;

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

task('app:fix_permission', function () {
    run('cd {{release_path}} && find . -type f -print0 | xargs -0 chmod 644');
    run('cd {{release_path}} && find . -type d -print0 | xargs -0 chmod 755');
});

task('app:dump_env', function () {
    $stage = null;
    if (input()->hasArgument('stage')) {
        $stage = input()->getArgument('stage');
    }
    $env = $stage == 'production' ? 'prod' : $stage;
    run('cd {{release_path}} && composer dump-env ' . $env);
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'database:migrate');
before('deploy:symlink', 'app:fix_permission');
#after('deploy:symlink', 'app:dump_env');


// Use rsync instead
set('rsync',[
    'exclude'      => [
        '/.git',
        '/deploy.php',
        '/vendor',
        '/var',
        '/node_modules',
        '/.idea'
    ],
    'exclude-file' => false,
    'include'      => [],
    'include-file' => false,
    'filter'       => [],
    'filter-file'  => false,
    'filter-perdir'=> false,
    'flags'        => 'rz', // Recursive, with compress
    'options'      => ['delete'],
    'timeout'      => 60,
]);
task('deploy:update_code')->disable();
after('deploy:update_code', 'rsync');

set('env', [
    'APP_ENV' => 'prod',
    'APP_SECRET' => '',
    'DATABASE_URL' => getenv('DATABASE_URL'),
    'MAILER_URL' => 'smtp://mail.syamsudin.dev:465'
###> symfony/swiftmailer-bundle ###
# For Gmail as a transport, use: "gmail://username:password@localhost"
# For a generic SMTP server, use: "smtp://localhost:25?encryption=&auth_mode="
# Delivery is disabled by default via "null://localhost"
###< symfony/swiftmailer-bundle ###
]);
