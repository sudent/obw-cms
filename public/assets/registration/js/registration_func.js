	/*  Wizard */
	jQuery(function ($) {
		"use strict";

		var saveRequest;

		$("#wizard_container").wizard({
			stepsWrapper: "#wrapped",
			beforeSelect: function (event, state) {
				if (!state.isMovingForward)
					return true;
				var inputs = $(this).wizard('state').step.find(':input');
				return !inputs.length || !!inputs.valid();
			},
		}).validate({
			errorPlacement: function (error, element) {
				if (element.is(':radio') || element.is(':checkbox')) {
					error.insertBefore(element.next());
				} else {
					error.insertAfter(element);
				}
			}
		});
		//  progress bar
		$("#progressbar").progressbar();
		$("#wizard_container").wizard({
			afterSelect: function (event, state) {
				$("#progressbar").progressbar("value", state.percentComplete);
				$("#location").text("(" + state.stepsComplete + "/" + state.stepsPossible + ")");

				var form = $('form#wrapped');
				var url = form.data('save-form-url');

				if (saveRequest != null) {
					saveRequest.abort();
					saveRequest = null;
				}

				saveRequest = $.ajax({
					type: "POST",
					url: url,
					data: form.serialize()
				})
				.done()
				.fail();
			}
		});
		// Validate select
		$('#wrapped').validate({
			ignore: [],
			rules: {
				select: {
					required: true
				}
			},
			errorPlacement: function (error, element) {
				if (element.is('select:hidden')) {
					error.insertAfter(element.next('.nice-select'));
				} else {
					error.insertAfter(element);
				}
			}
		});

		$('.qtyplus').on('click', function (e) {
			e.preventDefault();
			var fieldName = $(this).attr('name');
			var quantity = parseInt($('input[type="number"][name="' + fieldName + '"]').val());
			if (!isNaN(quantity)) {
				$('input[type="number"][name="' + fieldName + '"]').val(quantity + 1)
			} else {
				$('input[type="number"][name="' + fieldName + '"]').val(1)
			}
		});

		$('.qtyminus').on('click', function (e) {
			e.preventDefault();
			var fieldName = $(this).attr('name');
			var quantity = parseInt($('input[type="number"][name="' + fieldName + '"]').val());
			if (!isNaN(quantity) && quantity > 1) {
				$('input[type="number"][name="' + fieldName + '"]').val(quantity - 1)
			} else {
				$('input[type="number"][name="' + fieldName + '"]').val(1)
			}
		});

		$('#wizard_container').wizard({
			stepsWrapper: '#wrapped',
			transitions: {
				branchBatch: function (state, action) {
					var isSuperSenior = state.find('#frontend_pre_registration_student_batch').children("option:selected").data('super-senior');

					$('#amount-pre-registration').text('100');

					if (isSuperSenior === 0) {
						return 'join-sport-events-step';
					} else {
						return 'super-senior-step';
					}
				},
				branchSuperSenior: function (state, action) {
					if (state.find('#frontend_pre_registration_join_sport_events').prop('checked')) {
						return 'join-sport-events-step';
					} else {
						return 'contribution-step';
					}
				},
				end: function (state, action) {
					var form = $('form#wrapped');
					var url = form.attr('action');
					var $forwardBtn = $('button.forward');

					$forwardBtn.addClass('disabled-but-not-hidden');
					$forwardBtn.prop('disabled', true);
					$forwardBtn.find('.spinner-border').removeClass('d-none');

					$.ajax({
						type: "POST",
						url: url,
						data: form.serialize()
					}).done(function(data) {
						$('#end').html(data.html);

						$forwardBtn.prop('disabled', false);
						$forwardBtn.removeClass('disabled-but-not-hidden');
						$forwardBtn.find('.spinner-border').addClass('d-none');

						action("end");
					}).fail(function(data) {
						alert('Error while processing. Please try again.');

						$forwardBtn.prop('disabled', false);
						$forwardBtn.removeClass('disabled-but-not-hidden');
						$forwardBtn.find('.spinner-border').addClass('d-none');
					});

					return false;
				}
			}
		});
	});

	// Summary 
	function getVals(formControl) {
		var id = formControl.attr('id');
		switch (id) {

			case 'frontend_pre_registration_student_name':
				// Get the value for a input text
				var value = formControl.val();
				$("#full_name").text(value);
				break;

			case 'frontend_pre_registration_student_batch':
				// Get the value for a input text
				var value = formControl.find('option:selected').text();
				$("#batch").text(value);
				break;

			case 'frontend_pre_registration_student_house':
				// Get the value for a input text
				var value = formControl.find('option:selected').text();
				$("#house").text(value);
				break;

			 case 'frontend_pre_registration_student_contactNumber':
				// Get the value for a select
				var value = formControl.val();
				$("#contact_no").text(value);
				break;
		}
	}
