<?php

namespace App\Command;

use App\Entity\ObwProduct;
use App\Entity\ObwProductCategory;
use App\Entity\ObwSportEvent;
use App\Entity\StarBatch;
use App\Entity\StarHouse;
use App\Entity\User;
use Carbon\Carbon;
use Craue\ConfigBundle\Util\Config;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class InstallCommand extends Command
{
    protected static $defaultName = 'app:install';

    private $entityManager;
    private $config;

    public function __construct(EntityManagerInterface $entityManager, Config $config)
    {
        $this->entityManager = $entityManager;
        $this->config = $config;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $command = $this->getApplication()->find('doctrine:database:drop');

        $arguments = [
            'command' => 'doctrine:database:drop',
            '--force'    => true,
        ];

        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $output);
        $io->success('Database dropped');

        $command = $this->getApplication()->find('doctrine:database:create');

        $arguments = [
            'command' => 'doctrine:database:create',
        ];

        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $output);
        $io->success('Database created');

        $command = $this->getApplication()->find('doctrine:migrations:migrate');

        $arguments = [
            'command' => 'doctrine:migrations:migrate',
            '--allow-no-migration' => true
        ];

        $greetInput = new ArrayInput($arguments);
        $greetInput->setInteractive(false);
        $command->run($greetInput, $output);
        $io->success('Migrations ran');

        $command = $this->getApplication()->find('fos:user:create');

        $arguments = [
            'command' => 'fos:user:create',
            'username'    => 'admin@jebong.xyz',
            'email'  => 'admin@jebong.xyz',
            'password'  => 'admin123',
            '--super-admin' => true
        ];

        $greetInput = new ArrayInput($arguments);
        $command->run($greetInput, $output);
        $io->success('Super admin added');

        $admin = $this->entityManager->getRepository(User::class)->findOneBy(['email' => 'admin@jebong.xyz']);

        $starStartYear = 1957;
        $starStudyYear = 5;
        $startingYear = $starStartYear;
        $maxStartingYear = (int) date('Y');

        $generatedBatches = [];

        while ($startingYear <= $maxStartingYear) {
            $graduationYear = $startingYear + $starStudyYear - 1;

            $startDate = Carbon::createFromDate($startingYear, 1, 1);
            $endDate = Carbon::createFromDate($graduationYear, 12, 31);

            $batch = $this->entityManager->getRepository(StarBatch::class)->findOneBy([
                'yearEnd' => $endDate
            ]);

            if (!$batch) {
                $batch = new StarBatch();

                $batch->setYearStart($startDate);
                $batch->setYearEnd($endDate);
                $batch->setCreatedBy($admin);

                $this->entityManager->persist($batch);

                $generatedBatches[] = $batch;
            }

            $startingYear++;
        }

        $this->entityManager->flush();
        $io->success('Batch have been added');

        $houses = ['Blue', 'Black', 'Green', 'Red', 'White', 'Yellow'];

        foreach ($houses as $houseName) {
            $house = new StarHouse();
            $house->setName($houseName);
            $house->setCreatedBy($admin);

            $this->entityManager->persist($house);
        }

        $this->entityManager->flush();
        $io->success('House have been added');

        $sportEvents = ['Piala Presiden STAROBA', 'Boling STAROBA'];

        foreach ($sportEvents as $sportEventName) {
            $sportEvent = new ObwSportEvent();
            $sportEvent->setName($sportEventName);
            $sportEvent->setCreatedBy($admin);

            $this->entityManager->persist($sportEvent);
        }

        $this->entityManager->flush();
        $io->success('Sport events have been added');

        $categories = ['Registration Fees', 'Merchandise', 'Dinner Tables', 'Contributions'];

        $registrationFeeCategory = null;
        $merchandiseCategory = null;
        $dinnerTableCategory = null;
        $contributionCategory = null;

        foreach ($categories as $categoryName) {
            $category = new ObwProductCategory();
            $category->setName($categoryName);
            $category->setCreatedBy($admin);

            $this->entityManager->persist($category);

            if ($categoryName == 'Registration Fees') {
                $registrationFeeCategory = $category;
            } elseif ($categoryName == 'Merchandise') {
                $merchandiseCategory = $category;
            } elseif ($categoryName == 'Dinner Tables') {
                $dinnerTableCategory = $category;
            } elseif ($categoryName == 'Contributions') {
                $contributionCategory = $category;
            }
        }

        $this->entityManager->flush();
        $io->success('Product categories have been added');

        $products = [
            'Plant A Tree' => [
                'price' => '10000',
                'is_amount_varied' => false,
                'category' => $contributionCategory
            ],
            'OBW Team Jersey' => [
                'price' => '5000',
                'is_amount_varied' => false,
                'category' => $merchandiseCategory
            ],
            'OBW 2021 Pre-Registration Fees' => [
                'price' => '10000',
                'is_amount_varied' => false,
                'category' => $registrationFeeCategory
            ],
            'Dinner Table' => [
                'price' => '50000',
                'is_amount_varied' => false,
                'category' => $dinnerTableCategory
            ],
            'Fund For OBW 2021' => [
                'price' => '1000',
                'is_amount_varied' => true,
                'category' => $contributionCategory
            ],
        ];

        $obwTeamJerseyProduct = null;
        $obwPreRegistrationFeeProduct = null;
        $dinnerTableProduct = null;
        $fundForObwProduct = null;

        foreach ($products as $productName => $details) {
            $product = new ObwProduct();
            $product->setName($productName);
            $product->setPrice($details['price']);
            $product->setIsAmountVaried($details['is_amount_varied']);
            $product->setProductCategory($details['category']);
            $product->setCreatedBy($admin);

            $this->entityManager->persist($product);

            if ($productName == 'OBW Team Jersey') {
                $obwTeamJerseyProduct = $product;
            } elseif ($productName == 'OBW 2021 Pre-Registration Fees') {
                $obwPreRegistrationFeeProduct = $product;
            } elseif ($productName == 'Dinner Table') {
                $dinnerTableProduct = $product;
            } elseif ($productName == 'Fund For OBW 2021') {
                $fundForObwProduct = $product;
            }
        }

        $this->entityManager->flush();
        $io->success('Products have been added');

        $this->config->set('contribution_category', $contributionCategory->getId());
        $this->config->set('product_dinner_table_id', $dinnerTableProduct->getId());
        $this->config->set('product_fund_contribution', $fundForObwProduct->getId());
        $this->config->set('product_jersey_id', $obwTeamJerseyProduct->getId());
        $this->config->set('product_pre_registration_id', $obwPreRegistrationFeeProduct->getId());
        $this->config->set('super_senior_after_years', 15);
        $this->config->set('obw_date', '2021-08-31 00:00:00');
        $io->success('Configurations updated');

        $io->success('Site have been setup');

        return 0;
    }
}
