<?php

namespace App\Controller\Admin;

use App\Entity\ObwPreRegistration;
use App\Entity\ObwPreRegistrationOrder;
use App\Entity\ObwProduct;
use App\Entity\StarStudent;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class DashboardController extends AbstractController
{
    /**
     * @Route("/admin", name="admin_dashboard")
     */
    public function dashboard()
    {
        $totalPreRegistrations = $this->getDoctrine()->getRepository(ObwPreRegistration::class)->createQueryBuilder('x')
            ->select('count(x.id)')
            ->getQuery()
            ->getSingleScalarResult();

        $totalProducts = $this->getDoctrine()->getRepository(ObwProduct::class)->createQueryBuilder('x')
            ->select('count(x.id)')
            ->getQuery()
            ->getSingleScalarResult();

        $totalSales = $this->getDoctrine()->getRepository(ObwPreRegistrationOrder::class)->createQueryBuilder('x')
            ->select('sum(x.totalAmount)')
            ->where('x.status != :status_cancelled')
            ->setParameter('status_cancelled', ObwPreRegistrationOrder::STATUS_CANCELLED)
            ->getQuery()
            ->getSingleScalarResult();

        $totalStudents = $this->getDoctrine()->getRepository(StarStudent::class)->createQueryBuilder('x')
            ->select('count(x.id)')
            ->getQuery()
            ->getSingleScalarResult();

        return $this->render('admin/dashboard.html.twig', [
            'total_pre_registrations' => $totalPreRegistrations,
            'total_products' => $totalProducts,
            'total_sales' => $totalSales,
            'total_students' => $totalStudents
        ]);
    }

    /**
     * @Route("/change_language/{language}", name="change_language", requirements={"language": "en|ms"})
     */
    public function changeLanguage(Request $request, SessionInterface $session, $language)
    {
        $session->set('_locale', $language);

        if ($this->getUser() && $this->isGranted(User::ROLE_DEFAULT)) {
            $userProfile = $this->getUser()->getProfile();
            $userProfile->setLocale($language);

            $this->getDoctrine()->getManager()->persist($userProfile);
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/blank", name="blank")
     */
    public function blank()
    {
        return $this->render('blank/index.html.twig');
    }

    /**
     * @Route("/___debug", name="debug")
     */
    public function debug()
    {
        return $this->render('emails/ticket_comment_notification.html.twig', [
            'code' => '1234',
            'user' => $this->getUser(),
            'ticket' => 1234
        ]);
    }
}
