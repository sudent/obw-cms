<?php

namespace App\Controller\Admin;

use App\Entity\InviteCode;
use App\Entity\User;
use App\Form\InviteCodeType;
use App\Repository\InviteCodeRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/invite-codes")
 */
class InviteCodeController extends AbstractController
{
    /**
     * @Route("/", name="invite_code_index", methods={"GET"})
     */
    public function index(InviteCodeRepository $inviteCodeRepository): Response
    {
        return $this->render('admin/invite_code/index.html.twig', [
            'invite_codes' => $inviteCodeRepository->findBy([], ['isAlreadyUsed' => 'ASC']),
        ]);
    }

    /**
     * @Route("/generate", name="bulk_generate_invite_code", methods={"GET"})
     */
    public function bulkGenerateCodes(): Response
    {
        $totalToGenerate = 10;

        while ($totalToGenerate > 0) {
            $code = strtoupper(substr(str_shuffle(MD5(microtime())), 0, 10));

            $inviteCode = $this->getDoctrine()->getRepository(InviteCode::class)->findOneBy([
                'code' => $code
            ]);

            if (!$inviteCode) {
                $inviteCode = new InviteCode();
                $inviteCode->setCode($code);

                $this->getDoctrine()->getManager()->persist($inviteCode);

                $totalToGenerate--;
            }
        }

        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('invite_code_index');
    }

    /**
     * @Route("/new", name="invite_code_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $inviteCode = new InviteCode();
        $form = $this->createForm(InviteCodeType::class, $inviteCode);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($inviteCode);
            $entityManager->flush();

            return $this->redirectToRoute('invite_code_index');
        }

        return $this->render('admin/invite_code/new.html.twig', [
            'invite_code' => $inviteCode,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="invite_code_show", methods={"GET"})
     */
    public function show(InviteCode $inviteCode): Response
    {
        return $this->render('admin/invite_code/show.html.twig', [
            'invite_code' => $inviteCode,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="invite_code_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, InviteCode $inviteCode): Response
    {
        $form = $this->createForm(InviteCodeType::class, $inviteCode);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('invite_code_index', [
                'id' => $inviteCode->getId(),
            ]);
        }

        return $this->render('admin/invite_code/edit.html.twig', [
            'invite_code' => $inviteCode,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/send", name="send_invite_code", methods={"GET", "POST"})
     */
    public function sendInviteCode(Request $request, InviteCode $inviteCode, \Swift_Mailer $mailer, TranslatorInterface $translator, $defaultSenderEmail, $defaultSenderName): Response
    {
        $form = $this->createFormBuilder(null, ['translation_domain' => 'form']);
        $form->add('inviteCode', EntityType::class, [
            'class' => InviteCode::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('ic')
                    ->where('ic.isAlreadyUsed = false')
                    ->orderBy('ic.createdAt', 'ASC');
            },
            'disabled' => true,
            'constraints' => [
                new NotNull()
            ]
        ]);
        $form->add('emailAddress', EmailType::class, [
            'constraints' => [
                new NotBlank(),
                new Email()
            ]
        ]);

        $choices = [];

        if ($this->isGranted(User::ROLE_SUPER_ADMIN)) {
            $choices['Super Administrator'] = User::ROLE_SUPER_ADMIN;
        }

        $choices['Administrator'] = User::ROLE_ADMIN;
        $choices['Regular User'] = User::ROLE_DEFAULT;

        $form->add('role', ChoiceType::class, [
            'label' => 'User Type',
            'expanded' => true,
            'multiple' => false,
            'choices' => $choices,
            'attr' => ['class' => 'custom-controls-stacked'],
            'label_attr' => ['class' => 'radio-custom'],
            'required' => true
        ]);

        $form = $form->getForm();
        $form->setData(['inviteCode' => $inviteCode, 'emailAddress' => '', 'role' => User::ROLE_DEFAULT]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $emailAddress = $form->get('emailAddress')->getData();
            $role = $form->get('role')->getData();

            $message = (new \Swift_Message('Register to Login Into OBW 2021 Portal'))
                ->setFrom($defaultSenderEmail, $defaultSenderName)
                ->setTo($emailAddress)
                ->setBody(
                    $this->renderView(
                        'emails/invite_code.html.twig',
                        ['code' => $inviteCode->getCode(), 'email' => $emailAddress]
                    ),
                    'text/html'
                )
            ;

            $mailer->send($message);

            $inviteCode->setSentTo($emailAddress);
            $inviteCode->setIsSent(true);
            $inviteCode->setSentOn(new \DateTime());
            $inviteCode->setRole($role);

            $this->getDoctrine()->getManager()->persist($inviteCode);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('invite_code_index');
        }

        return $this->render('admin/invite_code/send.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/resend", name="resend_invite_code", methods={"GET", "POST"})
     */
    public function resendInviteCode(Request $request, InviteCode $inviteCode, \Swift_Mailer $mailer, TranslatorInterface $translator, $defaultSenderEmail, $defaultSenderName): Response
    {

        $message = (new \Swift_Message('Register to Login Into OBW 2021 Portal'))
            ->setFrom($defaultSenderEmail, $defaultSenderName)
            ->setTo($inviteCode->getSentTo())
            ->setBody(
                $this->renderView(
                // templates/emails/registration.html.twig
                    'emails/invite_code.html.twig',
                    ['code' => $inviteCode->getCode(), 'email' => $inviteCode->getSentTo()]
                ),
                'text/html'
            )
        ;

        $mailer->send($message);

        $inviteCode->setSentOn(new \DateTime());

        $this->getDoctrine()->getManager()->persist($inviteCode);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('invite_code_index');
    }

    /**
     * @Route("/{id}", name="invite_code_delete", methods={"DELETE"})
     */
    public function delete(Request $request, InviteCode $inviteCode): Response
    {
        if ($this->isCsrfTokenValid('delete'.$inviteCode->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($inviteCode);
            $entityManager->flush();
        }

        return $this->redirectToRoute('invite_code_index');
    }
}
