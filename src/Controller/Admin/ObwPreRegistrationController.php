<?php

namespace App\Controller\Admin;

use App\Datatable\ObwPreRegistrationDatatable;
use App\Entity\ObwPreRegistration;
use App\Entity\ObwPreRegistrationOrderStatusLog;
use App\Entity\ObwProduct;
use App\Entity\ObwProductCategory;
use App\Form\ObwPreRegistrationType;
use App\Repository\ObwPreRegistrationRepository;
use App\Settings\ObwPreRegistrationSetting;
use Craue\ConfigBundle\Util\Config;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * @Route("/admin/pre-registration")
 */
class ObwPreRegistrationController extends AbstractController
{
    /**
     * @Route("/", name="obw_pre_registration_index", methods={"GET"})
     */
    public function index(DatatableFactory $factory, DatatableResponse $datatableResponse, Request $request): Response
    {
        $datatable = $factory->create(ObwPreRegistrationDatatable::class);
        $datatable->buildDatatable();

        if ($request->isXmlHttpRequest()) {
            $datatableResponse->setDatatable($datatable);
            $datatableResponse->getDatatableQueryBuilder();

            return $datatableResponse->getResponse();
        }

        return $this->render('admin/obw_pre_registration/index.html.twig', array(
            'datatable' => $datatable,
        ));
    }

    /**
     * @Route("/new", name="obw_pre_registration_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $obwPreRegistration = new ObwPreRegistration();
        $form = $this->createForm(ObwPreRegistrationType::class, $obwPreRegistration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($obwPreRegistration);
            $entityManager->flush();

            return $this->redirectToRoute('obw_pre_registration_index');
        }

        return $this->render('admin/obw_pre_registration/new.html.twig', [
            'obw_pre_registration' => $obwPreRegistration,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="obw_pre_registration_show", methods={"GET"}, requirements={"id"="\d+"}, defaults={
     *         "_menu_key": "view_pre_registrations"
     *     }))
     */
    public function show(ObwPreRegistration $obwPreRegistration): Response
    {
        $lastStatusUpdate = $this->getDoctrine()->getRepository(ObwPreRegistrationOrderStatusLog::class)
            ->findBy(['obwPreRegistrationOrder' => $obwPreRegistration->getPreRegistrationOrder()], ['createdAt' => 'DESC'], 1);

        return $this->render('admin/obw_pre_registration/show.html.twig', [
            'obw_pre_registration' => $obwPreRegistration,
            'student' => $obwPreRegistration->getStudent(),
            'order' => $obwPreRegistration->getPreRegistrationOrder(),
            'last_status_update' => count($lastStatusUpdate) === 0 ? null : reset($lastStatusUpdate)->getCreatedAt()
        ]);
    }

    /**
     * @Route("/{id}/update-status/{status}", name="obw_pre_registration_update_status", methods={"GET"}, requirements={"status"="^(new|processing|completed|cancelled)$"})
     */
    public function updateStatus(ObwPreRegistration $obwPreRegistration, $status): Response
    {
        $order = $obwPreRegistration->getPreRegistrationOrder();
        $oldStatus = $order->getStatus();

        $order->setStatus($status);

        $statusLog = new ObwPreRegistrationOrderStatusLog();
        $statusLog->setOldStatus($oldStatus);
        $statusLog->setNewStatus($status);

        $order->addStatusLog($statusLog);

        $this->getDoctrine()->getManager()->persist($order);

        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success', 'Submission status successfully updated.');

        return $this->redirectToRoute('obw_pre_registration_show', ['id' => $obwPreRegistration->getId()]);
    }

    /**
     * @Route("/{id}/edit", name="obw_pre_registration_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ObwPreRegistration $obwPreRegistration): Response
    {
        $form = $this->createForm(ObwPreRegistrationType::class, $obwPreRegistration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('obw_pre_registration_index');
        }

        return $this->render('admin/obw_pre_registration/edit.html.twig', [
            'obw_pre_registration' => $obwPreRegistration,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="obw_pre_registration_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ObwPreRegistration $obwPreRegistration): Response
    {
        if ($this->isCsrfTokenValid('delete'.$obwPreRegistration->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($obwPreRegistration);
            $entityManager->flush();
        }

        return $this->redirectToRoute('obw_pre_registration_index');
    }

    /**
     * @Route("/configure", name="obw_pre_registration_configure", methods={"GET","POST"})
     */
    public function configure(Request $request, Config $config): Response
    {
        $obwPreRegistrationSettings = $config->getBySection(ObwPreRegistrationSetting::SECTION);

        foreach ($obwPreRegistrationSettings as $setting => $value) {
            $settingDefs = ObwPreRegistrationSetting::getDefs($setting);

            if ($settingDefs['type'] == 'entity') {
                $repository = $this->getDoctrine()->getRepository($settingDefs['class']);
                $value = $value ? $repository->find($value) : null;
            } elseif ($settingDefs['type'] == 'date') {
                $value = $value ? new \DateTime($value) : new \DateTime();
            }

            $obwPreRegistrationSettings[$setting] = $value;
        }

        $form = $this->createFormBuilder();

        foreach($obwPreRegistrationSettings as $setting => $value) {
            $settingDefs = ObwPreRegistrationSetting::getDefs($setting);

            if ($settingDefs['type'] == 'entity') {
                $form->add($setting, EntityType::class, [
                    'label' => $settingDefs['label'],
                    'class' => $settingDefs['class'],
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                        new NotNull()
                    ],
                    'placeholder' => $settingDefs['placeholder']
                ]);
            } elseif ($settingDefs['type'] == 'date') {
                $form->add($setting, DateType::class, [
                    'label' => $settingDefs['label'],
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                        new NotNull()
                    ],
                    'years' => range(date('Y'), date('Y') + 5),
                ]);
            } elseif ($settingDefs['type'] == 'number') {
                $form->add($setting, NumberType::class, [
                    'label' => $settingDefs['label'],
                    'required' => true,
                    'constraints' => [
                        new NotBlank(),
                        new NotNull()
                    ],
                ]);
            }
        }

        $form = $form->getForm();
        $form->setData($obwPreRegistrationSettings);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $settings = $form->getData();

            foreach ($settings as $setting => $value) {
                $settingDefs = ObwPreRegistrationSetting::getDefs($setting);

                if ($settingDefs['type'] == 'entity') {
                    $config->set($setting, $value->getId());
                } elseif ($settingDefs['type'] == 'date' && $value instanceof \DateTime) {
                    $config->set($setting, $value->format('Y-m-d 00:00:00'));
                } else {
                    $config->set($setting, $value);
                }
            }

            $this->addFlash('success', 'Settings have been saved');

            return $this->redirectToRoute('obw_pre_registration_configure');
        } else {
            $this->addFlash('danger', 'Settings is not valid. Please check all setting value is set and valid.');
        }

        return $this->render('admin/obw_pre_registration/configure.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
