<?php

namespace App\Controller\Admin;

use App\Datatable\ObwProductCategoryDatatable;
use App\Datatable\ObwProductDatatable;
use App\Entity\ObwProductCategory;
use App\Form\ObwProductCategoryType;
use App\Repository\ObwProductCategoryRepository;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Tetranz\Select2EntityBundle\Service\AutocompleteService;

/**
 * @Route("/admin/product/category")
 */
class ObwProductCategoryController extends AbstractController
{
    /**
     * @Route("/", name="obw_product_category_index", methods={"GET"})
     */
    public function index(DatatableFactory $factory, DatatableResponse $datatableResponse, Request $request): Response
    {
        $datatable = $factory->create(ObwProductCategoryDatatable::class);
        $datatable->buildDatatable();

        if ($request->isXmlHttpRequest()) {
            $datatableResponse->setDatatable($datatable);
            $datatableResponse->getDatatableQueryBuilder();

            return $datatableResponse->getResponse();
        }

        return $this->render('admin/obw_product_category/index.html.twig', array(
            'datatable' => $datatable,
        ));
    }

    /**
     * @Route("/new", name="obw_product_category_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $obwProductCategory = new ObwProductCategory();
        $form = $this->createForm(ObwProductCategoryType::class, $obwProductCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($obwProductCategory);
            $entityManager->flush();

            return $this->redirectToRoute('obw_product_category_index');
        }

        return $this->render('admin/obw_product_category/new.html.twig', [
            'obw_product_category' => $obwProductCategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="obw_product_category_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ObwProductCategory $obwProductCategory): Response
    {
        $form = $this->createForm(ObwProductCategoryType::class, $obwProductCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('obw_product_category_index');
        }

        return $this->render('admin/obw_product_category/edit.html.twig', [
            'obw_product_category' => $obwProductCategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="obw_product_category_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ObwProductCategory $obwProductCategory): Response
    {
        if ($this->isCsrfTokenValid('delete'.$obwProductCategory->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($obwProductCategory);
            $entityManager->flush();
        }

        return $this->redirectToRoute('obw_product_category_index');
    }

    /**
     * @param Request $request
     *
     * @param AutocompleteService $as
     * @return JsonResponse
     * @Route("/autocomplete", name="obw_product_category_autocomplete")
     */
    public function autocomplete(Request $request, AutocompleteService $as)
    {
        $maxResults = 20;

        $term = $request->get('q');
        $offset = ($request->get('page', 1) - 1) * 20;

        $productCategoryRepository = $this->getDoctrine()->getRepository(ObwProductCategory::class);

        $count = $productCategoryRepository->countTotalByName($term);
        $paginationResults = $productCategoryRepository->findByName($term, $offset);

        $result = ['results' => null, 'more' => $count > ($offset + $maxResults)];

        $result['results'] = array_map(function ($item) {
            /** @var ObwProductCategory $item */
            return ['id' => $item->getId(), 'text' => $item->getName()];
        }, $paginationResults);

        return new JsonResponse($result);
    }
}
