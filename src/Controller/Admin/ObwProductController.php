<?php

namespace App\Controller\Admin;

use App\Datatable\ObwProductDatatable;
use App\Entity\ObwProduct;
use App\Form\ObwProductType;
use App\Repository\ObwProductRepository;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/product")
 */
class ObwProductController extends AbstractController
{
    /**
     * @Route("/", name="obw_product_index", methods={"GET"})
     */
    public function index(DatatableFactory $factory, DatatableResponse $datatableResponse, Request $request): Response
    {
        $datatable = $factory->create(ObwProductDatatable::class);
        $datatable->buildDatatable();

        if ($request->isXmlHttpRequest()) {
            $datatableResponse->setDatatable($datatable);
            $datatableResponse->getDatatableQueryBuilder();

            return $datatableResponse->getResponse();
        }

        return $this->render('admin/obw_product/index.html.twig', array(
            'datatable' => $datatable,
        ));
    }

    /**
     * @Route("/new", name="obw_product_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $obwProduct = new ObwProduct();
        $form = $this->createForm(ObwProductType::class, $obwProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($obwProduct);
            $entityManager->flush();

            return $this->redirectToRoute('obw_product_index');
        }

        return $this->render('admin/obw_product/new.html.twig', [
            'obw_product' => $obwProduct,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="obw_product_show", methods={"GET"})
     */
    public function show(ObwProduct $obwProduct): Response
    {
        return $this->render('admin/obw_product/show.html.twig', [
            'obw_product' => $obwProduct,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="obw_product_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ObwProduct $obwProduct): Response
    {
        $form = $this->createForm(ObwProductType::class, $obwProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('obw_product_index');
        }

        return $this->render('admin/obw_product/edit.html.twig', [
            'obw_product' => $obwProduct,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="obw_product_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ObwProduct $obwProduct): Response
    {
        if ($this->isCsrfTokenValid('delete'.$obwProduct->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($obwProduct);
            $entityManager->flush();
        }

        return $this->redirectToRoute('obw_product_index');
    }
}
