<?php

namespace App\Controller\Admin;

use App\Datatable\ObwSportEventDatatable;
use App\Entity\ObwSportEvent;
use App\Form\ObwSportEventType;
use App\Repository\ObwSportEventRepository;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/sport-event")
 */
class ObwSportEventController extends AbstractController
{
    /**
     * @Route("/", name="obw_sport_event_index", methods={"GET"})
     */
    public function index(DatatableFactory $factory, DatatableResponse $datatableResponse, Request $request): Response
    {
        $datatable = $factory->create(ObwSportEventDatatable::class);
        $datatable->buildDatatable();

        if ($request->isXmlHttpRequest()) {
            $datatableResponse->setDatatable($datatable);
            $datatableResponse->getDatatableQueryBuilder();

            return $datatableResponse->getResponse();
        }

        return $this->render('admin/obw_sport_event/index.html.twig', array(
            'datatable' => $datatable,
        ));
    }

    /**
     * @Route("/new", name="obw_sport_event_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $obwSportEvent = new ObwSportEvent();
        $form = $this->createForm(ObwSportEventType::class, $obwSportEvent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($obwSportEvent);
            $entityManager->flush();

            return $this->redirectToRoute('obw_sport_event_index');
        }

        return $this->render('admin/obw_sport_event/new.html.twig', [
            'obw_sport_event' => $obwSportEvent,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="obw_sport_event_show", methods={"GET"})
     */
    public function show(ObwSportEvent $obwSportEvent): Response
    {
        return $this->render('admin/obw_sport_event/show.html.twig', [
            'obw_sport_event' => $obwSportEvent,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="obw_sport_event_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ObwSportEvent $obwSportEvent): Response
    {
        $form = $this->createForm(ObwSportEventType::class, $obwSportEvent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('obw_sport_event_index');
        }

        return $this->render('admin/obw_sport_event/edit.html.twig', [
            'obw_sport_event' => $obwSportEvent,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="obw_sport_event_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ObwSportEvent $obwSportEvent): Response
    {
        if ($this->isCsrfTokenValid('delete'.$obwSportEvent->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($obwSportEvent);
            $entityManager->flush();
        }

        return $this->redirectToRoute('obw_sport_event_index');
    }
}
