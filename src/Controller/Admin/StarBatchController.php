<?php

namespace App\Controller\Admin;

use App\Datatable\StarBatchDatatable;
use App\Entity\StarBatch;
use App\Form\StarBatchType;
use App\Repository\StarBatchRepository;
use Carbon\Carbon;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Tetranz\Select2EntityBundle\Service\AutocompleteService;

/**
 * @Route("/admin/batch")
 */
class StarBatchController extends AbstractController
{
    /**
     * @Route("/", name="star_batch_index", methods={"GET"})
     */
    public function index(DatatableFactory $factory, DatatableResponse $datatableResponse, Request $request): Response
    {
        $datatable = $factory->create(StarBatchDatatable::class);
        $datatable->buildDatatable();

        if ($request->isXmlHttpRequest()) {
            $datatableResponse->setDatatable($datatable);
            $datatableResponse->getDatatableQueryBuilder();

            return $datatableResponse->getResponse();
        }

        return $this->render('admin/star_batch/index.html.twig', array(
            'datatable' => $datatable,
        ));
    }

    /**
     * @Route("/new", name="star_batch_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $starBatch = new StarBatch();
        $form = $this->createForm(StarBatchType::class, $starBatch);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($starBatch);
            $entityManager->flush();

            return $this->redirectToRoute('star_batch_index');
        }

        return $this->render('admin/star_batch/new.html.twig', [
            'star_batch' => $starBatch,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="star_batch_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, StarBatch $starBatch): Response
    {
        $form = $this->createForm(StarBatchType::class, $starBatch);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('star_batch_index');
        }

        return $this->render('admin/star_batch/edit.html.twig', [
            'star_batch' => $starBatch,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="star_batch_delete", methods={"DELETE"})
     */
    public function delete(Request $request, StarBatch $starBatch): Response
    {
        if ($this->isCsrfTokenValid('delete'.$starBatch->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($starBatch);
            $entityManager->flush();
        }

        return $this->redirectToRoute('star_batch_index');
    }

    /**
     * @param Request $request
     *
     * @param AutocompleteService $as
     * @return JsonResponse
     * @Route("/autocomplete", name="star_batch_autocomplete")
     */
    public function autocomplete(Request $request, AutocompleteService $as)
    {
        $maxResults = 20;

        $term = $request->get('q');
        $offset = ($request->get('page', 1) - 1) * 20;

        $assetRepository = $this->getDoctrine()->getRepository(StarBatch::class);

        $count = $assetRepository->countTotalByName($term);
        $paginationResults = $assetRepository->findByName($term, $offset);

        $result = ['results' => null, 'more' => $count > ($offset + $maxResults)];

        $result['results'] = array_map(function ($item) {
            /** @var StarBatch $item */
            return ['id' => $item->getId(), 'text' => $item->getName()];
        }, $paginationResults);

        return new JsonResponse($result);
    }

    /**
     * @Route("/generate", name="bulk_generate_batch", methods={"GET"})
     */
    public function bulkGenerateBatch(): Response
    {
        $starStartYear = 1957;
        $starStudyYear = 5;
        $startingYear = $starStartYear;
        $maxStartingYear = (int) date('Y');

        $generatedBatches = [];

        while ($startingYear <= $maxStartingYear) {
            $graduationYear = $startingYear + $starStudyYear - 1;

            $startDate = Carbon::createFromDate($startingYear, 1, 1);
            $endDate = Carbon::createFromDate($graduationYear, 12, 31);

            $batch = $this->getDoctrine()->getRepository(StarBatch::class)->findOneBy([
                'yearEnd' => $endDate
            ]);

            if (!$batch) {
                $batch = new StarBatch();

                $batch->setYearStart($startDate);
                $batch->setYearEnd($endDate);

                $this->getDoctrine()->getManager()->persist($batch);

                $generatedBatches[] = $batch;
            }

            $startingYear++;
        }

        $this->getDoctrine()->getManager()->flush();

        if (count($generatedBatches) > 0) {
            foreach ($generatedBatches as $batch) {
                $this->addFlash('success', 'Generated batch ' . $batch->__toString());
            }
        } else {
            $this->addFlash('success', 'No batch generated. Every batch up until this year is already added.');
        }

        return $this->redirectToRoute('star_batch_index');
    }
}
