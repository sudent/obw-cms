<?php

namespace App\Controller\Admin;

use App\Datatable\StarHouseDatatable;
use App\Entity\StarHouse;
use App\Form\StarHouseType;
use App\Repository\StarHouseRepository;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/house")
 */
class StarHouseController extends AbstractController
{
    /**
     * @Route("/", name="star_house_index", methods={"GET"})
     */
    public function index(DatatableFactory $factory, DatatableResponse $datatableResponse, Request $request): Response
    {
        $datatable = $factory->create(StarHouseDatatable::class);
        $datatable->buildDatatable();

        if ($request->isXmlHttpRequest()) {
            $datatableResponse->setDatatable($datatable);
            $datatableResponse->getDatatableQueryBuilder();

            return $datatableResponse->getResponse();
        }

        return $this->render('admin/star_house/index.html.twig', array(
            'datatable' => $datatable,
        ));
    }

    /**
     * @Route("/new", name="star_house_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $starHouse = new StarHouse();
        $form = $this->createForm(StarHouseType::class, $starHouse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($starHouse);
            $entityManager->flush();

            return $this->redirectToRoute('star_house_index');
        }

        return $this->render('admin/star_house/new.html.twig', [
            'star_house' => $starHouse,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="star_house_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, StarHouse $starHouse): Response
    {
        $form = $this->createForm(StarHouseType::class, $starHouse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('star_house_index');
        }

        return $this->render('admin/star_house/edit.html.twig', [
            'star_house' => $starHouse,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="star_house_delete", methods={"DELETE"})
     */
    public function delete(Request $request, StarHouse $starHouse): Response
    {
        if ($this->isCsrfTokenValid('delete'.$starHouse->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($starHouse);
            $entityManager->flush();
        }

        return $this->redirectToRoute('star_house_index');
    }
}
