<?php

namespace App\Controller\Admin;

use App\Datatable\StarStudentDatatable;
use App\Entity\StarStudent;
use App\Form\StarStudentType;
use App\Repository\StarStudentRepository;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/student")
 */
class StarStudentController extends AbstractController
{
    /**
     * @Route("/", name="star_student_index", methods={"GET"})
     */
    public function index(DatatableFactory $factory, DatatableResponse $datatableResponse, Request $request): Response
    {
        $datatable = $factory->create(StarStudentDatatable::class);
        $datatable->buildDatatable();

        if ($request->isXmlHttpRequest()) {
            $datatableResponse->setDatatable($datatable);
            $datatableResponse->getDatatableQueryBuilder();

            return $datatableResponse->getResponse();
        }

        return $this->render('admin/star_student/index.html.twig', array(
            'datatable' => $datatable,
        ));
    }

    /**
     * @Route("/new", name="star_student_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $starStudent = new StarStudent();
        $form = $this->createForm(StarStudentType::class, $starStudent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($starStudent);
            $entityManager->flush();

            return $this->redirectToRoute('star_student_index');
        }

        return $this->render('admin/star_student/new.html.twig', [
            'star_student' => $starStudent,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="star_student_show", methods={"GET"})
     */
    public function show(StarStudent $starStudent): Response
    {
        return $this->render('admin/star_student/show.html.twig', [
            'star_student' => $starStudent,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="star_student_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, StarStudent $starStudent): Response
    {
        $form = $this->createForm(StarStudentType::class, $starStudent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('star_student_index');
        }

        return $this->render('admin/star_student/edit.html.twig', [
            'star_student' => $starStudent,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="star_student_delete", methods={"DELETE"})
     */
    public function delete(Request $request, StarStudent $starStudent): Response
    {
        if ($this->isCsrfTokenValid('delete'.$starStudent->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($starStudent);
            $entityManager->flush();
        }

        return $this->redirectToRoute('star_student_index');
    }
}
