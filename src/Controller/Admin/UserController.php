<?php

namespace App\Controller\Admin;

use App\Datatable\UserDatatable;
use App\Entity\User;
use App\Entity\UserProfile;
use App\Form\UserType;
use FOS\UserBundle\Model\UserManagerInterface;
use Ramsey\Uuid\Uuid;
use Sg\DatatablesBundle\Datatable\DatatableFactory;
use Sg\DatatablesBundle\Response\DatatableResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use YoHang88\LetterAvatar\LetterAvatar;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/admin")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/users", name="user_index")
     */
    public function index(DatatableFactory $factory, DatatableResponse $datatableResponse, Request $request): Response
    {
        $datatable = $factory->create(UserDatatable::class);
        $datatable->buildDatatable();

        if ($request->isXmlHttpRequest()) {
            $datatableResponse->setDatatable($datatable);
            $datatableResponse->getDatatableQueryBuilder();

            return $datatableResponse->getResponse();
        }

        return $this->render('admin/user/index.html.twig', array(
            'datatable' => $datatable,
        ));
    }

    /**
     * @Route("/users/add", name="user_add", defaults={
     *         "_menu_key": "manage_users"
     *     })
     * @param Request $request
     * @param UserManagerInterface $userManager
     * @param $userAvatarPath
     * @return RedirectResponse|Response
     */
    public function new(Request $request, UserManagerInterface $userManager)
    {
        /** @var User $user */
        $user = $userManager->createUser();
        $user->setEnabled(true);

        $form = $this->createForm(UserType::class, $user, ['currentUser' => $this->getUser()]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($user->getRole()) {
                $user->setRoles([$user->getRole()]);
            }

            $userManager->updateUser($user);

            return $this->redirectToRoute('user_index');
        }

        return $this->render('admin/user/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/users/{id}/edit", name="user_edit", defaults={
     *         "_menu_key": "manage_users"
     *     })
     *
     * @Security("user != userBeingEdited && is_granted(userBeingEdited.getRoles()[0]) && roles[0] != userBeingEdited.getRoles()[0]")
     *
     * @param Request $request
     * @param UserManagerInterface $userManager
     * @param AccessDecisionManagerInterface $accessDecisionManager
     * @param User $userBeingEdited
     * @return RedirectResponse|Response
     */
    public function edit(
        Request $request,
        UserManagerInterface $userManager,
        AccessDecisionManagerInterface $accessDecisionManager,
        User $userBeingEdited
    ) {
        $user = $userBeingEdited;

        $token = new UsernamePasswordToken(
            $user,
            null,
            'main',
            $user->getRoles()
        );

        if ($accessDecisionManager->decide($token, array(User::ROLE_SUPER_ADMIN))) {
            $user->setRole(User::ROLE_SUPER_ADMIN);
        } elseif ($accessDecisionManager->decide($token, array(User::ROLE_ADMIN))) {
            $user->setRole(User::ROLE_ADMIN);
        } else {
            $user->setRole(User::ROLE_DEFAULT);
        }

        $previousRole = $user->getRole();

        $form = $this->createForm(UserType::class, $user, [
            'currentUser' => $this->getUser()
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($user->getRole() && $user->getRole() != $previousRole) {
                $user->setRoles([$user->getRole()]);
            }

            $userManager->updateUser($user);

            return $this->redirectToRoute('user_index');
        }

        return $this->render('admin/user/edit.html.twig', array(
            'form' => $form->createView(),
            'user' => $user
        ));
    }

    /**
     * @Route("/users/{id}/remove", name="user_remove")
     */
    public function remove(Request $request, UserManagerInterface $userManager, $id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        $userManager->deleteUser($user);

        return $this->redirectToRoute('user_index');
    }
}
