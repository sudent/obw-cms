<?php

namespace App\Controller;

use App\Entity\ObwPreRegistration;
use App\Entity\ObwPreRegistrationOrder;
use App\Entity\ObwPreRegistrationOrderItem;
use App\Entity\ObwProduct;
use App\Entity\ObwProductCategory;
use App\Entity\ObwSportEvent;
use App\Entity\StarBatch;
use App\Entity\StarHouse;
use App\Entity\StarStudent;
use App\Form\FrontendPreRegistrationType;
use App\Settings\ObwPreRegistrationSetting;
use Carbon\Carbon;
use Carbon\CarbonInterface;
use Craue\ConfigBundle\Util\Config;
use RandomLib\Factory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\EmailValidator;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotBlankValidator;

/**
 * @Route("/")
 */
class OBWRegistrationController extends AbstractController
{
    /**
     * @Route("/", name="obw_pre_registration")
     */
    public function index(Request $request, Config $config)
    {
        $obwPreRegistrationSettings = $config->getBySection(ObwPreRegistrationSetting::SECTION);

        foreach ($obwPreRegistrationSettings as $setting => $value) {
            if (!isset($obwPreRegistrationSettings[$setting])) {
                return $this->render('obw_registration/unavailable.html.twig');
            }
        }

        $obwProductRepository = $this->getDoctrine()->getRepository(ObwProduct::class);

        $obwTeamJersey = $obwProductRepository->find($obwPreRegistrationSettings[ObwPreRegistrationSetting::PRODUCT_JERSEY_ID['name']]);
        $dinnerTable = $obwProductRepository->find($obwPreRegistrationSettings[ObwPreRegistrationSetting::PRODUCT_DINNER_TABLE_ID['name']]);
        $preRegistrationFee = $obwProductRepository->find($obwPreRegistrationSettings[ObwPreRegistrationSetting::PRODUCT_PRE_REGISTRATION_ID['name']]);
        $contributionCategory = $this->getDoctrine()->getRepository(ObwProductCategory::class)->find(
            $obwPreRegistrationSettings[ObwPreRegistrationSetting::CONTRIBUTION_CATEGORY['name']]
        );

        $data = [
            'reserve_table' => false,
            'join_sport_events' => false,
            'buy_jersey' => false,
            'jerseys' => [],
            'contributions' => []
        ];

        if ($request->query->has('debug') && $request->query->get('debug') == 1) {
            $obwPreRegistration = new ObwPreRegistration();

            $student = new StarStudent();
            $student->setName('Syamsudin Abd Aziz');
            $student->setBatch($this->getDoctrine()->getRepository(StarBatch::class)->findOneBy(['name' => '1997 - 2001']));
            $student->setHouse($this->getDoctrine()->getRepository(StarHouse::class)->findOneBy(['name' => 'White']));
            $student->setContactNumber('0133842208');
            $obwPreRegistration->setStudent($student);

            $order = new ObwPreRegistrationOrder();
            $obwPreRegistration->setPreRegistrationOrder($order);

            $orderItem = new ObwPreRegistrationOrderItem();
            $orderItem->setProduct($obwTeamJersey);
            $orderItem->setPrice($obwTeamJersey->getPrice());
            $orderItem->setQuantity(1);
            $orderItem->setOptions(['style' => 'men_fitted', 'size' => 'l']);
            $orderItem->setTotalAmount($orderItem->getQuantity() * $orderItem->getPrice());
            $order->addOrderItem($orderItem);

            $orderItem = new ObwPreRegistrationOrderItem();
            $orderItem->setProduct($obwTeamJersey);
            $orderItem->setPrice($obwTeamJersey->getPrice());
            $orderItem->setQuantity(1);
            $orderItem->setOptions(['style' => 'unisex_long_sleeve', 'size' => 'xl']);
            $orderItem->setTotalAmount($orderItem->getQuantity() * $orderItem->getPrice());
            $order->addOrderItem($orderItem);

            $jerseyQuantity = 2;

            $orderItem = new ObwPreRegistrationOrderItem();
            $orderItem->setProduct($dinnerTable);
            $orderItem->setPrice($dinnerTable->getPrice());
            $orderItem->setQuantity(3);
            $orderItem->setTotalAmount($orderItem->getQuantity() * $orderItem->getPrice());
            $order->addOrderItem($orderItem);

            $dinnerTableQuantity = $orderItem->getQuantity();

            $contributions = $this->getDoctrine()->getRepository(ObwProduct::class)->findBy([
                'productCategory' => $contributionCategory,
                'isAmountVaried' => false
            ]);

            foreach ($contributions as $contribution) {
                $orderItem = new ObwPreRegistrationOrderItem();
                $orderItem->setProduct($contribution);
                $orderItem->setPrice($contribution->getPrice());
                $orderItem->setQuantity(1);
                $orderItem->setTotalAmount($orderItem->getQuantity() * $orderItem->getPrice());
                $order->addOrderItem($orderItem);
            }

            $sportEvents = $this->getDoctrine()->getRepository(ObwSportEvent::class)->findAll();
            $joinSportEvent = count($sportEvents) > 0 ? true : false;

            foreach ($sportEvents as $sportEvent) {
                $obwPreRegistration->addSportEvent($sportEvent);
            }

            $contributeAmount = '750';

            $data = [
                'student' => $student,
                'reserve_table' => true,
                'reserve_table_quantity' => 3,
                'join_sport_events' => $joinSportEvent,
                'buy_jersey' => true,
                'jersey_quantities' => $jerseyQuantity,
                'contribute_amount' => $contributeAmount
            ];

            foreach ($obwPreRegistration->getSportEvents() as $sportEvent) {
                $data['sport_events'][] = $sportEvent;
            }

            foreach ($obwPreRegistration->getPreRegistrationOrder()->getOrderItems() as $orderItem) {
                if ($orderItem->getProduct()->getName() == $obwTeamJersey->getName()) {
                    if (count($orderItem->getOptions()) > 0
                        && isset($orderItem->getOptions()['style'])
                        && isset($orderItem->getOptions()['size'])
                    ) {
                        $data['jerseys'][] = [
                            'style' => $orderItem->getOptions()['style'],
                            'size' => $orderItem->getOptions()['size']
                        ];
                    }
                } elseif ($orderItem->getProduct()->getProductCategory()->getName() == $contributionCategory->getName()) {
                    $data['contributions'][] = $orderItem->getProduct();
                }
            }
        }

        $form = $this->createForm(FrontendPreRegistrationType::class, $data, [
            'contribution_category' => $contributionCategory
        ]);

        if ($request->getSession()->has('form_data')) {
            $data = $request->getSession()->get('form_data');

            $form->submit($data);
        }

        return $this->render('obw_registration/index.html.twig', [
            'form' => $form->createView(),
            'jersey_price' => $obwTeamJersey->getPriceDivided(),
            'dinner_table_price' => $dinnerTable->getPriceDivided(),
            'pre_registration_fee' => $preRegistrationFee->getPriceDivided()
        ]);
    }

    /**
     * @Route("/pre-submit", name="obw_pre_registration_pre_submit", methods={"POST"})
     */
    public function preSubmit(Request $request, Config $config)
    {
        $obwPreRegistrationSettings = $config->getBySection(ObwPreRegistrationSetting::SECTION);

        foreach ($obwPreRegistrationSettings as $setting => $value) {
            if (!isset($obwPreRegistrationSettings[$setting])) {
                return $this->render('obw_registration/unavailable.html.twig');
            }
        }

        $obwProductRepository = $this->getDoctrine()->getRepository(ObwProduct::class);

        $obwTeamJersey = $obwProductRepository->find($obwPreRegistrationSettings[ObwPreRegistrationSetting::PRODUCT_JERSEY_ID['name']]);
        $dinnerTable = $obwProductRepository->find($obwPreRegistrationSettings[ObwPreRegistrationSetting::PRODUCT_DINNER_TABLE_ID['name']]);
        $preRegistrationFee = $obwProductRepository->find($obwPreRegistrationSettings[ObwPreRegistrationSetting::PRODUCT_PRE_REGISTRATION_ID['name']]);
        $contributeFundProduct = $obwProductRepository->find($obwPreRegistrationSettings[ObwPreRegistrationSetting::PRODUCT_FUND_CONTRIBUTION['name']]);
        $contributionCategory = $this->getDoctrine()->getRepository(ObwProductCategory::class)->find(
            $obwPreRegistrationSettings[ObwPreRegistrationSetting::CONTRIBUTION_CATEGORY['name']]
        );

        $form = $this->createForm(FrontendPreRegistrationType::class, null, [
            'contribution_category' => $contributionCategory
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            /** @var StarStudent $student */
            $student = $data['student'];

            $obwPreRegistration = new ObwPreRegistration();

            $existingStudent = $this->getDoctrine()
                ->getRepository(StarStudent::class)
                ->getExistingStudent($student);

            if ($existingStudent) {
                $student = $existingStudent;
            }

            $obwPreRegistration->setStudent($student);

            $order = new ObwPreRegistrationOrder();

            $orderItem = new ObwPreRegistrationOrderItem();
            $orderItem->setProduct($preRegistrationFee);
            $orderItem->setQuantity(1);
            $orderItem->setPrice($preRegistrationFee->getPrice());
            $order->addOrderItem($orderItem);

            if ($data['join_sport_events']) {
                $sportEvents = $data['sport_events'];

                foreach ($sportEvents as $sportEvent) {
                    $obwPreRegistration->addSportEvent($sportEvent);
                }

                if ($data['buy_jersey'] && count($data['jerseys']) > 0) {
                    foreach ($data['jerseys'] as $jersey) {
                        $orderItem = new ObwPreRegistrationOrderItem();
                        $orderItem->setProduct($obwTeamJersey);
                        $orderItem->setQuantity(1);
                        $orderItem->setPrice($obwTeamJersey->getPrice());

                        $options = [
                            'style' => $jersey['style'],
                            'size' => $jersey['size']
                        ];

                        $orderItem->setOptions($options);

                        $order->addOrderItem($orderItem);
                    }
                }
            }

            if ($data['reserve_table'] && $data['reserve_table_quantity'] > 0) {
                $orderItem = new ObwPreRegistrationOrderItem();
                $orderItem->setProduct($dinnerTable);
                $orderItem->setQuantity($data['reserve_table_quantity']);
                $orderItem->setPrice($dinnerTable->getPrice());

                $order->addOrderItem($orderItem);
            }

            if (count($data['contributions']) > 0) {
                /** @var ObwProduct $contribution */
                foreach ($data['contributions'] as $contribution) {
                    $orderItem = new ObwPreRegistrationOrderItem();
                    $orderItem->setProduct($contribution);
                    $orderItem->setQuantity(1);
                    $orderItem->setPrice($contribution->getPrice());

                    $order->addOrderItem($orderItem);
                }
            }

            if ($data['contribute_amount'] > 0) {
                $orderItem = new ObwPreRegistrationOrderItem();
                $orderItem->setProduct($contributeFundProduct);
                $orderItem->setQuantity(1);
                $orderItem->setPrice($data['contribute_amount'] * 100);

                $order->addOrderItem($orderItem);
            }

            // Calculate all item total and order total
            $totalAmount = 0;
            foreach ($order->getOrderItems() as $orderItem) {
                $amount = $orderItem->getPrice() * $orderItem->getQuantity();
                $orderItem->setTotalAmount($amount);

                $totalAmount += $amount;
            }

            $order->setTotalAmount($totalAmount);

            $obwPreRegistration->setPreRegistrationOrder($order);

            if ($form->get('submit')->isClicked()) {
                $this->getDoctrine()->getManager()->persist($obwPreRegistration);
                $this->getDoctrine()->getManager()->flush();

                if ($request->getSession()->has('form_data')) {
                    $request->getSession()->remove('form_data');
                }

//                $emailForm = $this->getEmailForm();
//
//                return $this->render('obw_registration/payment_complete.html.twig', [
//                    'obw_pre_registration' => $obwPreRegistration,
//                    'student' => $obwPreRegistration->getStudent(),
//                    'order' => $obwPreRegistration->getPreRegistrationOrder(),
//                    'email_form' => $emailForm->createView()
//                ]);
//                $request->getSession()->getFlashBag()->get('preRegistrationId', []);
//                $this->addFlash('preRegistrationId', $obwPreRegistration->getId());

                return $this->redirectToRoute('obw_pre_registration_complete', ['code' => urlencode($obwPreRegistration->getCode())]);
            } else {
                // Render summary for response
                $html = $this->renderView('obw_registration/_summary.html.twig', [
                    'student' => $student,
                    'order' => $order
                ]);

                return new JsonResponse(['html' => $html]);
            }
        } else {
            if ($form->get('submit')->isClicked()) {
                return $this->render('obw_registration/index.html.twig', [
                    'form' => $form->createView(),
                    'jersey_price' => $obwTeamJersey->getPriceDivided(),
                    'dinner_table_price' => $dinnerTable->getPriceDivided(),
                    'pre_registration_fee' => $preRegistrationFee->getPriceDivided()
                ]);
            } else {
                return new JsonResponse(['status' => 'KO'], 400);
            }
        }
    }

    /**
     * @Route("/complete/{code}", name="obw_pre_registration_complete")
     */
    public function paymentComplete(Config $config, $code)
    {
        $obwPreRegistration = $this->getDoctrine()->getRepository(ObwPreRegistration::class)->findOneBy([
            'code' => urldecode($code)
        ]);

        if (!$obwPreRegistration) {
            throw $this->createNotFoundException();
        }

        $obwDate = $config->get(ObwPreRegistrationSetting::OBW_DATE['name']);

        $emailForm = $this->getEmailForm();

        return $this->render('obw_registration/payment_complete.html.twig', [
            'obw_pre_registration' => $obwPreRegistration,
            'student' => $obwPreRegistration->getStudent(),
            'order' => $obwPreRegistration->getPreRegistrationOrder(),
            'email_form' => $emailForm->createView(),
            'obw_date_difference' => Carbon::createFromFormat('Y-m-d H:i:s', $obwDate)->diffForHumans(
                null, CarbonInterface::DIFF_ABSOLUTE, false, 3
            )
        ]);
    }

    /**
     * @Route("/send-email/{code}", name="obw_pre_registration_email_order_details", methods={"POST"})
     */
    public function sendEmail(Request $request, \Swift_Mailer $mailer, $code)
    {
        $obwPreRegistration = $this->getDoctrine()->getRepository(ObwPreRegistration::class)->findOneBy([
            'code' => urldecode($code)
        ]);

        if (!$obwPreRegistration) {
            throw $this->createNotFoundException();
        }

        $emailForm = $this->getEmailForm();

        $emailForm->handleRequest($request);

        if ($emailForm->isSubmitted() && $emailForm->isValid()){
            $data = $emailForm->getData();

            $message = (new \Swift_Message('Order #' . sprintf('%08d', $obwPreRegistration->getId())))
                ->setFrom('noreply@jebong.xyz', 'OBW 2021 by STAR 9701')
                ->setTo($data['email'], $obwPreRegistration->getStudent()->getName())
                ->setBody(
                    $this->renderView(
                        'emails/pre_registration_order_details.html.twig', [
                            'obw_pre_registration' => $obwPreRegistration,
                            'student' => $obwPreRegistration->getStudent(),
                            'order' => $obwPreRegistration->getPreRegistrationOrder(),
                        ]
                    ),
                    'text/html'
                )
            ;

            $mailer->send($message);

            return new JsonResponse(['status' => 'OK']);
        } else {
            return new JsonResponse(['status' => 'KO'], 400);
        }
    }

    /**
     * @Route("/render-email/{code}", name="render_email")
     */
    public function renderEmail(Request $request, $code)
    {
        $obwPreRegistration = $this->getDoctrine()->getRepository(ObwPreRegistration::class)->findOneBy([
            'code' => urldecode($code)
        ]);

        if (!$obwPreRegistration) {
            throw $this->createNotFoundException();
        }

        return $this->render('emails/pre_registration_order_details.html.twig', [
            'obw_pre_registration' => $obwPreRegistration,
            'student' => $obwPreRegistration->getStudent(),
            'order' => $obwPreRegistration->getPreRegistrationOrder(),
        ]);
    }

    /**
     * @Route("/about", name="obw_pre_registration_about")
     */
    public function about()
    {
        return $this->render('obw_registration/about.html.twig', [
        ]);
    }

    /**
     * @Route("/contact", name="obw_pre_registration_contact")
     */
    public function contact()
    {
        return $this->render('obw_registration/contact.html.twig', [
        ]);
    }

    /**
     * @Route("/save-form", name="obw_pre_registration_save", methods={"POST"})
     */
    public function save(Request $request, Config $config)
    {
        $contributionCategory = $this->getDoctrine()->getRepository(ObwProductCategory::class)->find(
            $config->get(ObwPreRegistrationSetting::CONTRIBUTION_CATEGORY['name'])
        );

        $form = $this->createForm(FrontendPreRegistrationType::class, null, [
            'contribution_category' => $contributionCategory
        ]);

        $form->handleRequest($request);

        if ($request->request->has('frontend_pre_registration')) {
            $request->getSession()->set('form_data', $request->request->get('frontend_pre_registration'));
        }

        return new JsonResponse(['status' => 'OK']);
    }

    private function getEmailForm()
    {
        $emailForm = $this->createFormBuilder();
        $emailForm
            ->add('email', EmailType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Email()
                ]
            ]);

        return $emailForm->getForm();
    }
}
