<?php

namespace App\Datatable\Column;

use Sg\DatatablesBundle\Datatable\Column\AbstractColumn;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\FilterableTrait;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Helper;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StatusColumn extends AbstractColumn
{
    use FilterableTrait;

    public function getCellContentTemplate()
    {
        return 'admin/obw_pre_registration_order/_status_badge.html.twig';
    }

    public function renderSingleField(array &$row, array &$resultRow)
    {
        $path = Helper::getDataPropertyPath($this->data);

        $content = $this->twig->render(
            $this->getCellContentTemplate(),
            [
                'status' => $this->accessor->getValue($row, $path)
            ]
        );

        $this->accessor->setValue($resultRow, $path, $content);

        return $this;
    }

    public function renderToMany(array &$row, array &$resultRow)
    {
        // TODO: Implement renderToMany() method.
    }

    //-------------------------------------------------
    // Options
    //-------------------------------------------------

    /**
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'filter' => [TextFilter::class, []]
        ]);

        $resolver->setAllowedTypes('filter', 'array');

        return $this;
    }
}
