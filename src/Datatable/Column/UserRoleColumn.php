<?php

namespace App\Datatable\Column;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Sg\DatatablesBundle\Datatable\Column\AbstractColumn;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\FilterableTrait;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Helper;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserRoleColumn extends AbstractColumn
{
    use FilterableTrait;

    /** @var EntityManagerInterface */
    protected $em;

    public function getCellContentTemplate()
    {
        return 'datatables/column/user_role.html.twig';
    }

    public function renderSingleField(array &$row, array &$resultRow)
    {
        $path = Helper::getDataPropertyPath($this->data);

        $id = $row['id'];
        $user = $this->em->getRepository(User::class)->find($id);

        $data = $this->accessor->getValue($row, $path);

        $content = $this->twig->render(
            $this->getCellContentTemplate(),
            [
                'data' => reset($data),
                'user' => $user
            ]
        );

        $this->accessor->setValue($resultRow, $path, $content);

        return $this;
    }

    public function renderToMany(array &$row, array &$resultRow)
    {
        // TODO: Implement renderToMany() method.
    }

    //-------------------------------------------------
    // Options
    //-------------------------------------------------

    /**
     * @return $this
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefined(['em']);

        $resolver->setDefaults([
            'filter' => [TextFilter::class, []],
        ]);

        $resolver->setAllowedTypes('filter', 'array');
        $resolver->setAllowedTypes('em', EntityManagerInterface::class);

        return $this;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEm(): EntityManagerInterface
    {
        return $this->em;
    }

    /**
     * @param EntityManagerInterface $em
     */
    public function setEm(EntityManagerInterface $em): void
    {
        $this->em = $em;
    }
}
