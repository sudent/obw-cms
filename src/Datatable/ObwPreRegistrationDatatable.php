<?php

namespace App\Datatable;

use App\Datatable\Column\PriceColumn;
use App\Datatable\Column\StatusColumn;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;

use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Filter\Select2Filter;
use Sg\DatatablesBundle\Datatable\Style;

/**
 * Class ObwPreRegistrationDatatable
 *
 * @package App\Datatable\ObwPreRegistrationDatatable
 */
class ObwPreRegistrationDatatable extends AbstractDatatable
{
    /**
    * {@inheritdoc}
    *
    * @throws \Exception
    */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true
            //'language' => 'de'
        ));

        $this->ajax->set(array(
        ));

        $this->options->set(array(
            'individual_filtering' => true,
            'individual_filtering_position' => 'head',
            'order_cells_top' => true,
            'classes' => Style::BOOTSTRAP_4_STYLE,
            'search_delay' => 3000,
            'order' => [
                [0, 'desc']
            ],
        ));

        $this->features->set(array(
        ));

        $this->columnBuilder
            ->add('id', Column::class, array(
                'title' => 'Order #',
            ))
            ->add('preRegistrationOrder.status', StatusColumn::class, array(
                'title' => 'Status',
                'filter' => array(Select2Filter::class, array(
                    'placeholder' => 'Status',
                    'search_type' => 'eq',
                    'multiple' => true,
                    'select_options' => array(
                        'new' => 'New',
                        'processing' => 'Processing',
                        'completed' => 'Completed',
                        'cancelled' => 'Cancelled'
                    ),
                    'classes' => 'dt-select2'
                )),
            ))
            ->add('student.name', Column::class, array(
                'title' => 'Name',
            ))
            ->add('preRegistrationOrder.totalAmount', PriceColumn::class, array(
                'title' => 'Total Order Amount',
                'searchable' => false
            ))
            ->add('createdAt', DateTimeColumn::class, array(
                'filter' => array(DateRangeFilter::class, array(
                    'cancel_button' => true
                )),
                'title' => 'Order Date',
                'timeago' => true
            ))
            ->add(null, ActionColumn::class, array(
                'title' => $this->translator->trans('sg.datatables.actions.title'),
                'actions' => array(
                    array(
                        'route' => 'obw_pre_registration_show',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),
                        'label' => 'View',
                        'icon' => 'ion-ios-eye',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => 'View',
                            'class' => 'btn btn-primary btn-sm d-block d-md-inline-block mb-1 mb-md-0',
                            'role' => 'button'
                        ),
                    )
                )
        ));
    }

    /**
    * {@inheritdoc}
    */
    public function getEntity()
    {
        return 'App\Entity\ObwPreRegistration';
    }

    /**
    * {@inheritdoc}
    */
    public function getName()
    {
        return 'obwpreregistration_datatable';
    }
}
