<?php

namespace App\Datatable;

use App\Datatable\Column\PriceColumn;
use App\Entity\ObwProductCategory;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Filter\Select2Filter;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;

class ObwProductDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     *
     * @throws \Exception
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true
            //'language' => 'de'
        ));

        $this->ajax->set(array(
        ));

        $this->options->set(array(
            'individual_filtering' => true,
            'individual_filtering_position' => 'head',
            'order_cells_top' => true,
            'classes' => Style::BOOTSTRAP_4_STYLE,
            'search_delay' => 3000,
            'order' => array(
                array(0, 'desc')
            ),
        ));

        $this->features->set(array(
        ));

        $categories = $this->em->getRepository(ObwProductCategory::class)->findAll();

        $this->columnBuilder
            ->add('name', Column::class, array(
                'title' => 'Name',
            ))
            ->add('price', PriceColumn::class, array(
                'title' => 'Price',
            ))
            ->add('productCategory.name', Column::class, array(
                'title' => 'Category',
                'filter' => array(Select2Filter::class, array(
                    'placeholder' => 'Category',
                    'search_type' => 'eq',
                    'multiple' => true,
                    'select_options' => array('' => 'All') + $this->getOptionsArrayFromEntities($categories, 'name', 'name'),
                    'classes' => 'dt-select2'
                ))
            ))
            ->add('createdAt', DateTimeColumn::class, array(
                'title' => 'Added On',
            ))
            ->add('createdBy.profile.fullName', Column::class, array(
                'title' => 'Added By',
            ))
            ->add(null, ActionColumn::class, array(
                'title' => $this->translator->trans('sg.datatables.actions.title'),
                'actions' => array(
                    array(
                        'route' => 'obw_product_edit',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),
                        'label' => $this->translator->trans('sg.datatables.actions.edit'),
                        'icon' => 'ion ion-md-create',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('sg.datatables.actions.edit'),
                            'class' => 'btn btn-primary btn-sm',
                            'role' => 'button'
                        ),
                    )
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'App\Entity\ObwProduct';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'obw_product';
    }
}
