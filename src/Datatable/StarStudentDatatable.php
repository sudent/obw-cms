<?php

namespace App\Datatable;

use App\Entity\StarBatch;
use App\Entity\StarHouse;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Filter\Select2Filter;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;

class StarStudentDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     *
     * @throws \Exception
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true
            //'language' => 'de'
        ));

        $this->ajax->set(array(
        ));

        $this->options->set(array(
            'individual_filtering' => true,
            'individual_filtering_position' => 'head',
            'order_cells_top' => true,
            'classes' => Style::BOOTSTRAP_4_STYLE,
            'search_delay' => 3000,
            'order' => array(
                array(0, 'desc')
            ),
        ));

        $this->features->set(array(
        ));

        $batches = $this->em->getRepository(StarBatch::class)->findAll();
        $houses = $this->em->getRepository(StarHouse::class)->findAll();

        $this->columnBuilder
            ->add('name', Column::class, array(
                'title' => 'Name',
            ))
            ->add('batch.name', Column::class, array(
                'title' => 'Batch',
                'searchable' => true,
                'orderable' => true,
                'filter' => array(Select2Filter::class, array(
                    'placeholder' => 'Batch',
                    'search_type' => 'eq',
                    'multiple' => true,
                    'select_options' => array('' => 'All') + $this->getOptionsArrayFromEntities($batches, 'name', 'name'),
                    'classes' => 'dt-select2'
                ))
            ))
            ->add('house.name', Column::class, array(
                'title' => 'House',
                'filter' => array(Select2Filter::class, array(
                    'placeholder' => 'House',
                    'search_type' => 'eq',
                    'multiple' => true,
                    'select_options' => array('' => 'All') + $this->getOptionsArrayFromEntities($houses, 'name', 'name'),
                    'classes' => 'dt-select2'
                ))
            ))
            ->add('contactNumber', Column::class, array(
                'title' => 'Contact Number',
            ))
            ->add('createdAt', DateTimeColumn::class, array(
                'filter' => array(DateRangeFilter::class, array(
                    'cancel_button' => true
                )),
                'title' => 'Added',
                'timeago' => true,
                'width' => '150px'
            ))
            ->add(null, ActionColumn::class, array(
                'title' => $this->translator->trans('sg.datatables.actions.title'),
                'actions' => array(
                    array(
                        'route' => 'star_student_edit',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),
                        'label' => $this->translator->trans('sg.datatables.actions.edit'),
                        'icon' => 'ion ion-md-create',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('sg.datatables.actions.edit'),
                            'class' => 'btn btn-primary btn-sm',
                            'role' => 'button'
                        ),
                    )
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'App\Entity\StarStudent';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'star_student';
    }
}
