<?php

namespace App\Datatable;

use App\Datatable\Column\UserRoleColumn;
use App\Entity\User;
use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;

class UserDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     *
     * @throws \Exception
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true
            //'language' => 'de'
        ));

        $this->ajax->set(array(
        ));

        $this->options->set(array(
            'individual_filtering' => true,
            'individual_filtering_position' => 'head',
            'order_cells_top' => true,
            'classes' => Style::BOOTSTRAP_4_STYLE,
            'search_delay' => 3000,
            'order' => array(
                array(0, 'desc')
            ),
        ));

        $this->features->set(array(
        ));

        $this->columnBuilder
            ->add('profile.fullName', Column::class, array(
                'title' => 'Name',
            ))
            ->add('email', Column::class, array(
                'title' => 'Email',
            ))
            ->add('roles', UserRoleColumn::class, array(
                'title' => 'User Type',
                'em' => $this->em
            ))
            ->add('enabled', BooleanColumn::class, array(
                'title' => 'Status',
                'true_label' => 'Active',
                'false_label' => 'Inactive',
                'true_icon' => 'ion ion-md-checkmark-circle',
                'false_icon' => 'ion ion-md-remove-circle',
            ))
            ->add('lastLogin', DateTimeColumn::class, array(
                'title' => 'Last Logged In',
            ))
            ->add(null, ActionColumn::class, array(
                'title' => $this->translator->trans('sg.datatables.actions.title'),
                'actions' => array(
                    array(
                        'route' => 'user_edit',
                        'route_parameters' => array(
                            'id' => 'id'
                        ),
                        'render_if' => function ($row) {
                            $user = $this->securityToken->getToken()->getUser();
                            $role = count($row['roles']) == 1 ? $row['roles'][0] : User::ROLE_DEFAULT;

                            return $user->getId() != $row['id']
                                && $this->authorizationChecker->isGranted($role)
                                && $user->getRoles()[0] !== $role;
                        },
                        'label' => $this->translator->trans('sg.datatables.actions.edit'),
                        'icon' => 'ion ion-md-create',
                        'attributes' => array(
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('sg.datatables.actions.edit'),
                            'class' => 'btn btn-primary btn-sm',
                            'role' => 'button'
                        ),
                    )
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'App\Entity\User';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'user';
    }
}
