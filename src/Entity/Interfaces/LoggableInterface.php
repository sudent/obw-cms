<?php

namespace App\Entity\Interfaces;

interface LoggableInterface
{
    const NO_LABEL = 'no label set';

    public function getLabelForField($field);

    public function getLabelForAssociation($entity);
}
