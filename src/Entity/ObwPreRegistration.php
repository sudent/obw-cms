<?php

namespace App\Entity;

use App\Entity\Traits\BlameableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use RandomLib\Factory;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ObwPreRegistrationRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @Gedmo\Loggable
 */
class ObwPreRegistration
{
    use BlameableEntity;
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\StarStudent", inversedBy="obwPreRegistrations", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $student;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ObwSportEvent", cascade={"persist"})
     */
    private $sportEvents;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ObwPreRegistrationOrder", mappedBy="obwPreRegistration", cascade={"persist", "remove"})
     */
    private $preRegistrationOrder;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Gedmo\Slug(fields={"code"})
     */
    private $slug;

    public function __construct()
    {
        $this->sportEvents = new ArrayCollection();

        $factory = new Factory();
        $generator = $factory->getMediumStrengthGenerator();

        $this->code = $generator->generateString(32);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStudent(): ?StarStudent
    {
        return $this->student;
    }

    public function setStudent(?StarStudent $student): self
    {
        $this->student = $student;

        return $this;
    }

    /**
     * @return Collection|ObwSportEvent[]
     */
    public function getSportEvents(): Collection
    {
        return $this->sportEvents;
    }

    public function addSportEvent(ObwSportEvent $sportEvent): self
    {
        if (!$this->sportEvents->contains($sportEvent)) {
            $this->sportEvents[] = $sportEvent;
        }

        return $this;
    }

    public function removeSportEvent(ObwSportEvent $sportEvent): self
    {
        if ($this->sportEvents->contains($sportEvent)) {
            $this->sportEvents->removeElement($sportEvent);
        }

        return $this;
    }

    public function getPreRegistrationOrder(): ?ObwPreRegistrationOrder
    {
        return $this->preRegistrationOrder;
    }

    public function setPreRegistrationOrder(ObwPreRegistrationOrder $preRegistrationOrder): self
    {
        $this->preRegistrationOrder = $preRegistrationOrder;

        // set the owning side of the relation if necessary
        if ($preRegistrationOrder->getObwPreRegistration() !== $this) {
            $preRegistrationOrder->setObwPreRegistration($this);
        }

        return $this;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
