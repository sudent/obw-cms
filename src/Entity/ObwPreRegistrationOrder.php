<?php

namespace App\Entity;

use App\Entity\Traits\BlameableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ObwPreRegistrationOrderRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @Gedmo\Loggable
 */
class ObwPreRegistrationOrder
{
    use BlameableEntity;
    use TimestampableEntity;
    use SoftDeleteableEntity;

    const STATUS_NEM = 'new';
    const STATUS_PROCESSING = 'processing';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_COMPLETED = 'completed';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ObwPreRegistration", inversedBy="preRegistrationOrder", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $obwPreRegistration;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalAmount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ObwPreRegistrationOrderItem", mappedBy="preRegistrationOrder", cascade={"persist"})
     */
    private $orderItems;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ObwPreRegistrationOrderStatusLog", mappedBy="obwPreRegistrationOrder", cascade={"persist"})
     */
    private $statusLogs;

    public function __construct()
    {
        $this->orderItems = new ArrayCollection();
        $this->status = 'new';
        $this->statusLogs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getObwPreRegistration(): ?ObwPreRegistration
    {
        return $this->obwPreRegistration;
    }

    public function setObwPreRegistration(ObwPreRegistration $obwPreRegistration): self
    {
        $this->obwPreRegistration = $obwPreRegistration;

        return $this;
    }

    public function getTotalAmount(): ?int
    {
        return $this->totalAmount;
    }

    public function setTotalAmount(int $totalAmount): self
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|ObwPreRegistrationOrderItem[]
     */
    public function getOrderItems(): Collection
    {
        return $this->orderItems;
    }

    public function addOrderItem(ObwPreRegistrationOrderItem $orderItem): self
    {
        if (!$this->orderItems->contains($orderItem)) {
            $this->orderItems[] = $orderItem;
            $orderItem->setPreRegistrationOrder($this);
        }

        return $this;
    }

    public function removeOrderItem(ObwPreRegistrationOrderItem $orderItem): self
    {
        if ($this->orderItems->contains($orderItem)) {
            $this->orderItems->removeElement($orderItem);
            // set the owning side to null (unless already changed)
            if ($orderItem->getPreRegistrationOrder() === $this) {
                $orderItem->setPreRegistrationOrder(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ObwPreRegistrationOrderStatusLog[]
     */
    public function getStatusLogs(): Collection
    {
        return $this->statusLogs;
    }

    public function addStatusLog(ObwPreRegistrationOrderStatusLog $statusLog): self
    {
        if (!$this->statusLogs->contains($statusLog)) {
            $this->statusLogs[] = $statusLog;
            $statusLog->setObwPreRegistrationOrder($this);
        }

        return $this;
    }

    public function removeStatusLog(ObwPreRegistrationOrderStatusLog $statusLog): self
    {
        if ($this->statusLogs->contains($statusLog)) {
            $this->statusLogs->removeElement($statusLog);
            // set the owning side to null (unless already changed)
            if ($statusLog->getObwPreRegistrationOrder() === $this) {
                $statusLog->setObwPreRegistrationOrder(null);
            }
        }

        return $this;
    }
}
