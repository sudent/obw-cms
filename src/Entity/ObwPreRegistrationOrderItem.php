<?php

namespace App\Entity;

use App\Entity\Traits\BlameableEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ObwPreRegistrationOrderItemRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @Gedmo\Loggable
 */
class ObwPreRegistrationOrderItem
{
    use BlameableEntity;
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ObwPreRegistrationOrder", inversedBy="orderItems")
     */
    private $preRegistrationOrder;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ObwProduct")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $totalAmount;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $options = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPreRegistrationOrder(): ?ObwPreRegistrationOrder
    {
        return $this->preRegistrationOrder;
    }

    public function setPreRegistrationOrder(?ObwPreRegistrationOrder $preRegistrationOrder): self
    {
        $this->preRegistrationOrder = $preRegistrationOrder;

        return $this;
    }

    public function getProduct(): ?ObwProduct
    {
        return $this->product;
    }

    public function setProduct(?ObwProduct $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getTotalAmount(): ?int
    {
        return $this->totalAmount;
    }

    public function setTotalAmount(int $totalAmount): self
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    public function getOptions(): ?array
    {
        return $this->options;
    }

    public function setOptions(?array $options): self
    {
        $this->options = $options;

        return $this;
    }
}
