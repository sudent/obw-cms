<?php

namespace App\Entity;

use App\Entity\Traits\BlameableEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ObwPreRegistrationOrderStatusLogRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @Gedmo\Loggable
 */
class ObwPreRegistrationOrderStatusLog
{
    use BlameableEntity;
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $newStatus;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $oldStatus;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ObwPreRegistrationOrder", inversedBy="statusLogs")
     */
    private $obwPreRegistrationOrder;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNewStatus(): ?string
    {
        return $this->newStatus;
    }

    public function setNewStatus(string $newStatus): self
    {
        $this->newStatus = $newStatus;

        return $this;
    }

    public function getOldStatus(): ?string
    {
        return $this->oldStatus;
    }

    public function setOldStatus(?string $oldStatus): self
    {
        $this->oldStatus = $oldStatus;

        return $this;
    }

    public function getObwPreRegistrationOrder(): ?ObwPreRegistrationOrder
    {
        return $this->obwPreRegistrationOrder;
    }

    public function setObwPreRegistrationOrder(?ObwPreRegistrationOrder $obwPreRegistrationOrder): self
    {
        $this->obwPreRegistrationOrder = $obwPreRegistrationOrder;

        return $this;
    }
}
