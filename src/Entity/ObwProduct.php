<?php

namespace App\Entity;

use App\Entity\Traits\BlameableEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ObwProductRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @Gedmo\Loggable
 */
class ObwProduct
{
    use BlameableEntity;
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ObwProductCategory", inversedBy="products")
     */
    private $productCategory;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAmountVaried;

    public function __construct()
    {
        $this->isAmountVaried = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPriceDivided()
    {
        return $this->price / 100;
    }

    public function getProductCategory(): ?ObwProductCategory
    {
        return $this->productCategory;
    }

    public function setProductCategory(?ObwProductCategory $productCategory): self
    {
        $this->productCategory = $productCategory;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getIsAmountVaried(): ?bool
    {
        return $this->isAmountVaried;
    }

    public function setIsAmountVaried(bool $isAmountVaried): self
    {
        $this->isAmountVaried = $isAmountVaried;

        return $this;
    }
}
