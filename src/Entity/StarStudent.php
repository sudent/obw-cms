<?php

namespace App\Entity;

use App\Entity\Traits\BlameableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StarStudentRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @Gedmo\Loggable
 */
class StarStudent
{
    use BlameableEntity;
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contactNumber;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\StarBatch", inversedBy="students")
     */
    private $batch;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\StarHouse", inversedBy="students")
     */
    private $house;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ObwPreRegistration", mappedBy="student")
     */
    private $obwPreRegistrations;

    public function __construct()
    {
        $this->obwPreRegistrations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getContactNumber(): ?string
    {
        return $this->contactNumber;
    }

    public function setContactNumber(?string $contactNumber): self
    {
        $this->contactNumber = $contactNumber;

        return $this;
    }

    public function getBatch(): ?StarBatch
    {
        return $this->batch;
    }

    public function setBatch(?StarBatch $batch): self
    {
        $this->batch = $batch;

        return $this;
    }

    public function getHouse(): ?StarHouse
    {
        return $this->house;
    }

    public function setHouse(?StarHouse $house): self
    {
        $this->house = $house;

        return $this;
    }

    /**
     * @return Collection|ObwPreRegistration[]
     */
    public function getObwPreRegistrations(): Collection
    {
        return $this->obwPreRegistrations;
    }

    public function addObwPreRegistration(ObwPreRegistration $obwPreRegistration): self
    {
        if (!$this->obwPreRegistrations->contains($obwPreRegistration)) {
            $this->obwPreRegistrations[] = $obwPreRegistration;
            $obwPreRegistration->setStudent($this);
        }

        return $this;
    }

    public function removeObwPreRegistration(ObwPreRegistration $obwPreRegistration): self
    {
        if ($this->obwPreRegistrations->contains($obwPreRegistration)) {
            $this->obwPreRegistrations->removeElement($obwPreRegistration);
            // set the owning side to null (unless already changed)
            if ($obwPreRegistration->getStudent() === $this) {
                $obwPreRegistration->setStudent(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
