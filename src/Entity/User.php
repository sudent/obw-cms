<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use FOS\UserBundle\Model\User as BaseUser;
use App\Entity\Traits\BlameableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @Gedmo\Loggable
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{
    use BlameableEntity;
    use TimestampableEntity;
    use SoftDeleteableEntity;

    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UserProfile", cascade={"persist", "remove"}, inversedBy="user")
     */
    private $profile;

    private $role;

    private $inviteCode;

    public function __construct()
    {
        parent::__construct();

        $this->profile = new UserProfile();
        $this->role = null;
    }

    public function __toString()
    {
        return $this->getHumanizedName();
    }

    public function getHumanizedName()
    {
        return $this->getProfile() && $this->getProfile()->getFullName() ? $this->profile->getFullName() : $this->getUsername();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setEmail($email)
    {
        parent::setEmail($email);

        $this->setUsername($email);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return UserProfile
     */
    public function getProfile(): UserProfile
    {
        return $this->profile;
    }

    /**
     * @param UserProfile $profile
     */
    public function setProfile(UserProfile $profile): void
    {
        $this->profile = $profile;
    }

    /**
     * @return null
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param null $role
     */
    public function setRole($role): void
    {
        $this->role = $role;
    }

    public function getRoleHumanized()
    {
        $role = $this->getRoles();

        if (count($role) > 0) {
            $role = $role[0];

            if ($role == self::ROLE_ADMIN) {
                return 'Administrator';
            } elseif ($role == self::ROLE_SUPER_ADMIN) {
                return 'Super Administrator';
            }
        } else {
            return 'User';
        }
    }

    /**
     * @return mixed
     */
    public function getInviteCode()
    {
        return $this->inviteCode;
    }

    /**
     * @param mixed $inviteCode
     */
    public function setInviteCode($inviteCode): void
    {
        $this->inviteCode = $inviteCode;
    }
}
