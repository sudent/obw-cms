<?php

namespace App\EventSubscriber;

use App\Entity\InviteCode;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Ramsey\Uuid\Uuid;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use YoHang88\LetterAvatar\LetterAvatar;

class RegistrationSubscriber implements EventSubscriberInterface
{
    private $entityManager;
    private $urlGenerator;
    private $flashBag;
    private $requestStack;

    private $currentUsername;
    private $currentFullName;
    private $inviteCode;

    public function __construct(
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $urlGenerator,
        FlashBagInterface $flashBag,
        RequestStack $requestStack
    ) {
        $this->entityManager = $entityManager;
        $this->urlGenerator = $urlGenerator;
        $this->flashBag = $flashBag;
        $this->requestStack = $requestStack;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_INITIALIZE => 'onRegistrationInitialize',
            FOSUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess',
            FOSUserEvents::REGISTRATION_COMPLETED => 'onRegistrationCompleted'
        );
    }

    public function onRegistrationInitialize(GetResponseUserEvent $event)
    {
        /** @var User $user */
        $user = $event->getUser();

        $request = $this->requestStack->getCurrentRequest();

        if ($request->query->has('invite_code')) {
            $user->setInviteCode($request->query->get('invite_code'));
        }

        if ($request->query->has('email')) {
            $user->setEmail($request->query->get('email'));
        }
    }

    public function onRegistrationSuccess(FormEvent $event)
    {
        $form = $event->getForm();
        /** @var User $user */
        $user = $form->getData();

        $fullName = $user->getProfile()->getFullName();
        $avatar = new LetterAvatar($fullName ? $fullName : $user->getUsername());
        $fileName = str_replace('-', '', Uuid::uuid4()) . '.png';
        $filePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $fileName;
        $avatar->saveAs($filePath);

        if ($user->getInviteCode()) {
            $inviteCode = $this->entityManager->getRepository(InviteCode::class)->findOneBy(['code' => $user->getInviteCode()]);

            if ($inviteCode && $inviteCode->getRole()) {
                $user->addRole($inviteCode->getRole());
            }
        }

        $user->getProfile()->setImageFile(new UploadedFile($filePath, $fileName, null, null, true));

        $url = $this->urlGenerator->generate('admin_dashboard');

        $event->setResponse(new RedirectResponse($url));
    }

    public function onRegistrationCompleted(FilterUserResponseEvent $event)
    {
        $user = $event->getUser();

        if ($user->getInviteCode()) {
            $inviteCode = $this->entityManager->getRepository(InviteCode::class)->findOneBy(['code' => $user->getInviteCode()]);

            if ($inviteCode) {
                $inviteCode->setIsAlreadyUsed(true);
                $inviteCode->setUsedAt(new \DateTime());
                $inviteCode->setUser($user);

                $this->entityManager->persist($inviteCode);
                $this->entityManager->flush();
            }
        }
    }
}
