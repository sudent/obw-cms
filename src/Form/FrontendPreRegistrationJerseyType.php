<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FrontendPreRegistrationJerseyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('style', ChoiceType::class, [
                'choices' => [
                    'Men Fitted' => 'men_fitted',
                    'Women Fitted' => 'women_fitted',
                    'Unisex Long Sleeve' => 'unisex_long_sleeve',
                ],
                'placeholder' => 'Style',
                'attr' => [
                    'class' => 'small right'
                ]
            ])
            ->add('size', ChoiceType::class, [
                'choices' => [
                    'S' => 's',
                    'M' => 'm',
                    'L' => 'l',
                    'XL' => 'xl',
                ],
                'placeholder' => 'Size',
                'attr' => [
                    'class' => 'small right'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
