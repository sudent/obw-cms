<?php

namespace App\Form;

use App\Entity\StarBatch;
use App\Entity\StarHouse;
use App\Entity\StarStudent;
use App\Settings\ObwPreRegistrationSetting;
use Craue\ConfigBundle\Util\Config;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FrontendPreRegistrationStudentType extends AbstractType
{
    private $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $maxSuperSeniorYear = $this->config->get(ObwPreRegistrationSetting::SUPER_SENIOR_AFTER_YEARS['name']);

        $builder
            ->add('name', null, [
                'attr' => ['placeholder' => 'Full Name']
            ])
            ->add('contactNumber', null, [
                'attr' => ['placeholder' => 'Contact No']
            ])
            ->add('batch', EntityType::class, [
                'class' => StarBatch::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.yearStart', 'ASC');
                },
                'choice_attr' => function($val, $key, $index) use ($maxSuperSeniorYear) {
                    /** @var StarBatch $val */
                    $maxSuperSeniorYear = date('Y') - $maxSuperSeniorYear;
                    return ['data-super-senior' => $val->getYearStart()->format('Y') >= $maxSuperSeniorYear ? "0" : "1"];
                },
                'attr' => [
                    'class' => 'wide'
                ],
                'placeholder' => 'Batch',
                'required' => true
            ])
            ->add('house', EntityType::class, [
                'class' => StarHouse::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('h')
                        ->orderBy('h.name', 'ASC');
                },
                'attr' => [
                    'class' => 'wide'
                ],
                'placeholder' => 'House',
                'required' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => StarStudent::class,
            'required' => true
        ]);
    }
}
