<?php

namespace App\Form;

use App\Entity\ObwProduct;
use App\Entity\ObwProductCategory;
use App\Entity\ObwSportEvent;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FrontendPreRegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $contributionCategory = $options['contribution_category'];

        $builder
            ->add('student', FrontendPreRegistrationStudentType::class)
            ->add('sport_events', EntityType::class, [
                'class' => ObwSportEvent::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.name', 'ASC');
                },
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true
            ])
            ->add('contributions', EntityType::class, [
                'class' => ObwProduct::class,
                'query_builder' => function (EntityRepository $er) use ($contributionCategory) {
                    return $er->createQueryBuilder('p')
                        ->where('p.productCategory = :contribution_category')
                        ->andWhere('p.isAmountVaried = false')
                        ->setParameter('contribution_category', $contributionCategory)
                        ->orderBy('p.name', 'ASC');
                },
                'choice_label' => 'name',
                'expanded' => true,
                'multiple' => true,
                'choice_attr' => function($val, $key, $index) {
                    return ['data-price' => $val->getPrice()];
                },
            ])
            ->add('reserve_table', CheckboxType::class, [
                'label' => false,
                'required' => false
            ])
            ->add('join_sport_events', CheckboxType::class, [
                'required' => false
            ])
            ->add('buy_jersey', CheckboxType::class, [
                'required' => false,
            ])
            ->add('reserve_table_quantity', IntegerType::class, [
                'data' => isset($options['data']) && isset($options['data']['reserve_table_quantity']) ? $options['data']['reserve_table_quantity'] : 1,
                'attr' => [
                    'class' => 'qty'
                ]
            ])
            ->add('jerseys', CollectionType::class, [
                'entry_type' => FrontendPreRegistrationJerseyType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'entry_options' => ['label' => false],
            ])
            ->add('contribute_amount', RangeType::class, [
                'attr' => [
                    'min' => 0,
                    'max' => 1000,
                    'data-orientation' => 'horizontal',
                    'step' => 10
                ]
            ])
            ->add('jersey_quantities', IntegerType::class, [
                'data' => isset($options['data']) && isset($options['data']['jersey_quantities']) ? $options['data']['jersey_quantities'] : 1,
                'attr' => ['class' => 'qty']
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Make Payment',
                'attr' => ['class' => 'submit']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(['contribution_category']);

        $resolver->setDefaults([
            'contribution_category' => null
        ]);

        $resolver->setAllowedTypes('contribution_category', ObwProductCategory::class);
    }
}
