<?php

namespace App\Form;

use App\Entity\ObwPreRegistrationOrder;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ObwPreRegistrationOrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('totalAmount')
            ->add('status')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('deletedAt')
            ->add('obwPreRegistration')
            ->add('createdBy')
            ->add('updatedBy')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ObwPreRegistrationOrder::class,
        ]);
    }
}
