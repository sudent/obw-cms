<?php

namespace App\Form;

use App\Entity\ObwProduct;
use App\Entity\ObwProductCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class ObwProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('price', MoneyType::class, [
                'divisor' => 100,
                'currency' => 'MYR'
            ])
            ->add('productCategory', Select2EntityType::class, [
                'remote_route' => 'obw_product_category_autocomplete',
                'remote_params' => [],
                'class' => ObwProductCategory::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'minimum_input_length' => 0,
                'page_limit' => 20,
                'delay' => 250,
                'cache' => true,
                'cache_timeout' => 60000, // if 'cache' is true
                'language' => 'en',
                'placeholder' => 'Product Category',
                'label' => 'Product Category',
                'required' => true
            ])
            ->add('isAmountVaried')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ObwProduct::class,
        ]);
    }
}
