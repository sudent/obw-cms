<?php

namespace App\Form;

use App\Entity\StarBatch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StarBatchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('yearStart', null, [
                'years' => range(1957,date('Y')),
                'label' => 'Starting Year',
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'yyyy',
                'attr' => ['class' => 'batch-year'],
                'required' => true

            ])
            ->add('yearEnd', null, [
                'years' => range(1957,date('Y')),
                'label' => 'Graduating Year',
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'yyyy',
                'attr' => ['class' => 'batch-year'],
                'required' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => StarBatch::class,
        ]);
    }
}
