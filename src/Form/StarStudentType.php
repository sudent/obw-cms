<?php

namespace App\Form;

use App\Entity\StarBatch;
use App\Entity\StarStudent;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class StarStudentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('contactNumber', null, ['label' => 'Contact Number'])
            ->add('batch', Select2EntityType::class, [
                'remote_route' => 'star_batch_autocomplete',
                'remote_params' => [],
                'class' => StarBatch::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'minimum_input_length' => 0,
                'page_limit' => 20,
                'delay' => 250,
                'cache' => true,
                'cache_timeout' => 60000, // if 'cache' is true
                'language' => 'en',
                'placeholder' => 'Batch',
                'label' => 'Batch',
                'required' => true
            ])
            ->add('house')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => StarStudent::class,
        ]);
    }
}
