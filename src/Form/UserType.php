<?php

namespace App\Form;

use App\Entity\Organization;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class UserType extends AbstractType
{
    private $tokenStorage;
    private $authorizationChecker;

    public function __construct(TokenStorageInterface $tokenStorage, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tokenStorage = $this->tokenStorage;
        $authorizationChecker = $this->authorizationChecker;

        $builder
            ->add('email', EmailType::class, array('label' => 'Email Address', 'required' => true))
            ->add('profile', UserProfileType::class)
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'options' => array(
                    'attr' => array(
                        'autocomplete' => 'new-password',
                    ),
                ),
                'first_options' => array('label' => 'Password', 'required' => true),
                'second_options' => array('label' => 'Repeat Password', 'required' => true),
                'invalid_message' => 'Password must match',
                'required' => false
            ));

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($tokenStorage, $authorizationChecker) {
            $form = $event->getForm();
            /** @var User $data */
            $user = $event->getData();

            $choices = [];

            if ($authorizationChecker->isGranted(User::ROLE_SUPER_ADMIN, $tokenStorage->getToken()->getUser())) {
                $choices['Super Administrator'] = User::ROLE_SUPER_ADMIN;
            }

            $choices['Administrator'] = User::ROLE_ADMIN;
            $choices['User'] = User::ROLE_DEFAULT;

            $form->add('role', ChoiceType::class, [
                'label' => 'Role',
                'expanded' => true,
                'multiple' => false,
                'choices' => $choices,
                'attr' => ['class' => 'custom-controls-stacked'],
                'label_attr' => ['class' => 'radio-custom'],
                // Disable editing role for current user
                'disabled' => $user == $tokenStorage->getToken()->getUser() ? true : false,
                'help' => 'Unable to change role for your own account',
                'required' => true
            ]);

            $form->add('enabled', null, ['label' => 'Enable User Login?']);
        });

        $builder->remove('username');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'translation_domain' => 'form'
        ]);

        $resolver->setRequired(['currentUser']);
        $resolver->setAllowedTypes('currentUser', User::class);
    }
}
