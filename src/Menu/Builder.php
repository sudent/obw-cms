<?php
namespace App\Menu;

use App\Entity\User;
use Knp\Menu\FactoryInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class Builder
{
    private $factory;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->factory = $factory;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function createSidebarMenu(array $options)
    {
        $menu = $this->factory->createItem('root');

        $menu->setChildrenAttribute('class', 'sidenav-inner py-1');

        $menu->addChild('Dashboard', [
            'route' => 'admin_dashboard',
            'attributes' => [
                'icon' => 'ion ion-ios-home'
            ],
            'label' => 'Dashboard'
        ]);

        if ($this->authorizationChecker->isGranted(User::ROLE_ADMIN)) {
            $preRegistrationMenu = $menu->addChild('pre_registrations', ['label' => 'Pre-Registrations', 'attributes' => ['dropdown' => true, 'icon' => 'ion ion-ios-create']]);
            $preRegistrationMenu->addChild('view_pre_registrations', array('label' => 'View All Submissions', 'route' => 'obw_pre_registration_index'));
            $preRegistrationMenu->addChild('pre_registration_settings', array('label' => 'Settings', 'route' => 'obw_pre_registration_configure'));

            $obwItinerariesMenu = $menu->addChild('obw_itineraries', ['label' => 'OBW Itineraries', 'attributes' => ['dropdown' => true, 'icon' => 'ion ion-ios-list']]);
            $obwItinerariesMenu->addChild('manage_products', array('label' => 'Manage Products', 'route' => 'obw_product_index'));
            $obwItinerariesMenu->addChild('manage_product_category', array('label' => 'Manage Product Categories', 'route' => 'obw_product_category_index'));
            $obwItinerariesMenu->addChild('manage_sport_events', array('label' => 'Manage Sport Events', 'route' => 'obw_sport_event_index'));

            $starMenu = $menu->addChild('star', ['label' => 'STAR Management', 'attributes' => ['dropdown' => true, 'icon' => 'ion ion-ios-school']]);
            $starMenu->addChild('manage_students', array('label' => 'Manage Students', 'route' => 'star_student_index'));
            $starMenu->addChild('manage_house', array('label' => 'Manage Houses', 'route' => 'star_house_index'));
            $starMenu->addChild('manage_batch', array('label' => 'Manage Batch', 'route' => 'star_batch_index'));

            $adminMenu = $menu->addChild('administration', ['label' => 'Administration', 'attributes' => ['dropdown' => true, 'icon' => 'ion ion-ios-settings']]);
            $adminMenu->addChild('manage_users', ['label' => 'Manage Users', 'route' => 'user_index']);
            $adminMenu->addChild('user_invites', ['label' => 'User Invitations', 'route' => 'invite_code_index']);
        }

        return $menu;
    }
}
