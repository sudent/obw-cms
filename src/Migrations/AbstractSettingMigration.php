<?php

declare(strict_types=1);

namespace App\Migrations;

use Craue\ConfigBundle\Entity\Setting;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
abstract class AbstractSettingMigration extends AbstractMigration implements ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function up(Schema $schema) : void
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $settings = $this->getSettings();

        $settingsCount = 0;

        foreach ($settings as $section => $sectionSettitngs) {
            foreach ($sectionSettitngs as $settingDefs) {
                $setting = new Setting();
                $setting->setName($settingDefs['name']);
                $setting->setSection($section);

                $em->persist($setting);

                $settingsCount++;
            }
        }

        $em->flush();

        $this->write('Successfully added ' . $settingsCount . ' setting(s)');
    }

    public function down(Schema $schema) : void
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $settings = $this->getSettings();

        $settingRepository = $em->getRepository(Setting::class);
        $settingsCount = 0;

        foreach ($settings as $section => $sectionSettitngs) {
            foreach ($sectionSettitngs as $settingDefs) {
                $setting = $settingRepository->findOneBy(['name' => $settingDefs['name']]);

                if (!$setting) {
                    continue;
                }

                $em->remove($setting);

                $settingsCount++;
            }
        }

        $em->flush();

        $this->write('Successfully removed ' . $settingsCount . ' setting(s)');
    }

    abstract public function getSettings(): array ;
}
