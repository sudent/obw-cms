<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200305073834 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE obw_product (id INT AUTO_INCREMENT NOT NULL, product_category_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, price INT NOT NULL, INDEX IDX_2B5B93FABE6903FD (product_category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE star_house (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE star_batch (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, year_start DATE NOT NULL, year_end DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE obw_pre_registration (id INT AUTO_INCREMENT NOT NULL, student_id INT NOT NULL, INDEX IDX_A9E06CE0CB944F1A (student_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE obw_pre_registration_obw_sport_event (obw_pre_registration_id INT NOT NULL, obw_sport_event_id INT NOT NULL, INDEX IDX_4568C4B7A6E1D3EA (obw_pre_registration_id), INDEX IDX_4568C4B7BF166689 (obw_sport_event_id), PRIMARY KEY(obw_pre_registration_id, obw_sport_event_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE obw_pre_registration_order (id INT AUTO_INCREMENT NOT NULL, obw_pre_registration_id INT NOT NULL, total_amount INT NOT NULL, status VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_3541D91DA6E1D3EA (obw_pre_registration_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE star_student (id INT AUTO_INCREMENT NOT NULL, batch_id INT DEFAULT NULL, house_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, contact_number VARCHAR(255) DEFAULT NULL, INDEX IDX_20C3A120F39EBE7A (batch_id), INDEX IDX_20C3A1206BB74515 (house_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE obw_pre_registration_order_item (id INT AUTO_INCREMENT NOT NULL, pre_registration_order_id INT DEFAULT NULL, product_id INT NOT NULL, quantity INT NOT NULL, price INT NOT NULL, total_amount INT NOT NULL, INDEX IDX_527BD56414CCBB2B (pre_registration_order_id), INDEX IDX_527BD5644584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE obw_sport_event (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE obw_product_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE obw_product ADD CONSTRAINT FK_2B5B93FABE6903FD FOREIGN KEY (product_category_id) REFERENCES obw_product_category (id)');
        $this->addSql('ALTER TABLE obw_pre_registration ADD CONSTRAINT FK_A9E06CE0CB944F1A FOREIGN KEY (student_id) REFERENCES star_student (id)');
        $this->addSql('ALTER TABLE obw_pre_registration_obw_sport_event ADD CONSTRAINT FK_4568C4B7A6E1D3EA FOREIGN KEY (obw_pre_registration_id) REFERENCES obw_pre_registration (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE obw_pre_registration_obw_sport_event ADD CONSTRAINT FK_4568C4B7BF166689 FOREIGN KEY (obw_sport_event_id) REFERENCES obw_sport_event (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE obw_pre_registration_order ADD CONSTRAINT FK_3541D91DA6E1D3EA FOREIGN KEY (obw_pre_registration_id) REFERENCES obw_pre_registration (id)');
        $this->addSql('ALTER TABLE star_student ADD CONSTRAINT FK_20C3A120F39EBE7A FOREIGN KEY (batch_id) REFERENCES star_batch (id)');
        $this->addSql('ALTER TABLE star_student ADD CONSTRAINT FK_20C3A1206BB74515 FOREIGN KEY (house_id) REFERENCES star_house (id)');
        $this->addSql('ALTER TABLE obw_pre_registration_order_item ADD CONSTRAINT FK_527BD56414CCBB2B FOREIGN KEY (pre_registration_order_id) REFERENCES obw_pre_registration_order (id)');
        $this->addSql('ALTER TABLE obw_pre_registration_order_item ADD CONSTRAINT FK_527BD5644584665A FOREIGN KEY (product_id) REFERENCES obw_product (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE obw_pre_registration_order_item DROP FOREIGN KEY FK_527BD5644584665A');
        $this->addSql('ALTER TABLE star_student DROP FOREIGN KEY FK_20C3A1206BB74515');
        $this->addSql('ALTER TABLE star_student DROP FOREIGN KEY FK_20C3A120F39EBE7A');
        $this->addSql('ALTER TABLE obw_pre_registration_obw_sport_event DROP FOREIGN KEY FK_4568C4B7A6E1D3EA');
        $this->addSql('ALTER TABLE obw_pre_registration_order DROP FOREIGN KEY FK_3541D91DA6E1D3EA');
        $this->addSql('ALTER TABLE obw_pre_registration_order_item DROP FOREIGN KEY FK_527BD56414CCBB2B');
        $this->addSql('ALTER TABLE obw_pre_registration DROP FOREIGN KEY FK_A9E06CE0CB944F1A');
        $this->addSql('ALTER TABLE obw_pre_registration_obw_sport_event DROP FOREIGN KEY FK_4568C4B7BF166689');
        $this->addSql('ALTER TABLE obw_product DROP FOREIGN KEY FK_2B5B93FABE6903FD');
        $this->addSql('DROP TABLE obw_product');
        $this->addSql('DROP TABLE star_house');
        $this->addSql('DROP TABLE star_batch');
        $this->addSql('DROP TABLE obw_pre_registration');
        $this->addSql('DROP TABLE obw_pre_registration_obw_sport_event');
        $this->addSql('DROP TABLE obw_pre_registration_order');
        $this->addSql('DROP TABLE star_student');
        $this->addSql('DROP TABLE obw_pre_registration_order_item');
        $this->addSql('DROP TABLE obw_sport_event');
        $this->addSql('DROP TABLE obw_product_category');
    }
}
