<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200309061832 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE obw_product ADD created_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD updated_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL, ADD deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE obw_product ADD CONSTRAINT FK_2B5B93FADE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE obw_product ADD CONSTRAINT FK_2B5B93FA16FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_2B5B93FADE12AB56 ON obw_product (created_by)');
        $this->addSql('CREATE INDEX IDX_2B5B93FA16FE72E1 ON obw_product (updated_by)');
        $this->addSql('ALTER TABLE obw_pre_registration ADD created_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD updated_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL, ADD deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE obw_pre_registration ADD CONSTRAINT FK_A9E06CE0DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE obw_pre_registration ADD CONSTRAINT FK_A9E06CE016FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_A9E06CE0DE12AB56 ON obw_pre_registration (created_by)');
        $this->addSql('CREATE INDEX IDX_A9E06CE016FE72E1 ON obw_pre_registration (updated_by)');
        $this->addSql('ALTER TABLE obw_product_category ADD created_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD updated_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL, ADD deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE obw_product_category ADD CONSTRAINT FK_C6E2EE0FDE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE obw_product_category ADD CONSTRAINT FK_C6E2EE0F16FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_C6E2EE0FDE12AB56 ON obw_product_category (created_by)');
        $this->addSql('CREATE INDEX IDX_C6E2EE0F16FE72E1 ON obw_product_category (updated_by)');
        $this->addSql('ALTER TABLE obw_sport_event ADD created_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD updated_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL, ADD deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE obw_sport_event ADD CONSTRAINT FK_C1EBABEEDE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE obw_sport_event ADD CONSTRAINT FK_C1EBABEE16FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_C1EBABEEDE12AB56 ON obw_sport_event (created_by)');
        $this->addSql('CREATE INDEX IDX_C1EBABEE16FE72E1 ON obw_sport_event (updated_by)');
        $this->addSql('ALTER TABLE star_batch ADD created_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD updated_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL, ADD deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE star_batch ADD CONSTRAINT FK_F7FCBAF8DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE star_batch ADD CONSTRAINT FK_F7FCBAF816FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_F7FCBAF8DE12AB56 ON star_batch (created_by)');
        $this->addSql('CREATE INDEX IDX_F7FCBAF816FE72E1 ON star_batch (updated_by)');
        $this->addSql('ALTER TABLE star_house ADD created_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD updated_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL, ADD deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE star_house ADD CONSTRAINT FK_6822D1B1DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE star_house ADD CONSTRAINT FK_6822D1B116FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_6822D1B1DE12AB56 ON star_house (created_by)');
        $this->addSql('CREATE INDEX IDX_6822D1B116FE72E1 ON star_house (updated_by)');
        $this->addSql('ALTER TABLE star_student ADD created_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD updated_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL, ADD deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE star_student ADD CONSTRAINT FK_20C3A120DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE star_student ADD CONSTRAINT FK_20C3A12016FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_20C3A120DE12AB56 ON star_student (created_by)');
        $this->addSql('CREATE INDEX IDX_20C3A12016FE72E1 ON star_student (updated_by)');
        $this->addSql('ALTER TABLE obw_pre_registration_order ADD created_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD updated_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL, ADD deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE obw_pre_registration_order ADD CONSTRAINT FK_3541D91DDE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE obw_pre_registration_order ADD CONSTRAINT FK_3541D91D16FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_3541D91DDE12AB56 ON obw_pre_registration_order (created_by)');
        $this->addSql('CREATE INDEX IDX_3541D91D16FE72E1 ON obw_pre_registration_order (updated_by)');
        $this->addSql('ALTER TABLE obw_pre_registration_order_item ADD created_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD updated_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL, ADD deleted_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE obw_pre_registration_order_item ADD CONSTRAINT FK_527BD564DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE obw_pre_registration_order_item ADD CONSTRAINT FK_527BD56416FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_527BD564DE12AB56 ON obw_pre_registration_order_item (created_by)');
        $this->addSql('CREATE INDEX IDX_527BD56416FE72E1 ON obw_pre_registration_order_item (updated_by)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE obw_pre_registration DROP FOREIGN KEY FK_A9E06CE0DE12AB56');
        $this->addSql('ALTER TABLE obw_pre_registration DROP FOREIGN KEY FK_A9E06CE016FE72E1');
        $this->addSql('DROP INDEX IDX_A9E06CE0DE12AB56 ON obw_pre_registration');
        $this->addSql('DROP INDEX IDX_A9E06CE016FE72E1 ON obw_pre_registration');
        $this->addSql('ALTER TABLE obw_pre_registration DROP created_by, DROP updated_by, DROP created_at, DROP updated_at, DROP deleted_at');
        $this->addSql('ALTER TABLE obw_pre_registration_order DROP FOREIGN KEY FK_3541D91DDE12AB56');
        $this->addSql('ALTER TABLE obw_pre_registration_order DROP FOREIGN KEY FK_3541D91D16FE72E1');
        $this->addSql('DROP INDEX IDX_3541D91DDE12AB56 ON obw_pre_registration_order');
        $this->addSql('DROP INDEX IDX_3541D91D16FE72E1 ON obw_pre_registration_order');
        $this->addSql('ALTER TABLE obw_pre_registration_order DROP created_by, DROP updated_by, DROP created_at, DROP updated_at, DROP deleted_at');
        $this->addSql('ALTER TABLE obw_pre_registration_order_item DROP FOREIGN KEY FK_527BD564DE12AB56');
        $this->addSql('ALTER TABLE obw_pre_registration_order_item DROP FOREIGN KEY FK_527BD56416FE72E1');
        $this->addSql('DROP INDEX IDX_527BD564DE12AB56 ON obw_pre_registration_order_item');
        $this->addSql('DROP INDEX IDX_527BD56416FE72E1 ON obw_pre_registration_order_item');
        $this->addSql('ALTER TABLE obw_pre_registration_order_item DROP created_by, DROP updated_by, DROP created_at, DROP updated_at, DROP deleted_at');
        $this->addSql('ALTER TABLE obw_product DROP FOREIGN KEY FK_2B5B93FADE12AB56');
        $this->addSql('ALTER TABLE obw_product DROP FOREIGN KEY FK_2B5B93FA16FE72E1');
        $this->addSql('DROP INDEX IDX_2B5B93FADE12AB56 ON obw_product');
        $this->addSql('DROP INDEX IDX_2B5B93FA16FE72E1 ON obw_product');
        $this->addSql('ALTER TABLE obw_product DROP created_by, DROP updated_by, DROP created_at, DROP updated_at, DROP deleted_at');
        $this->addSql('ALTER TABLE obw_product_category DROP FOREIGN KEY FK_C6E2EE0FDE12AB56');
        $this->addSql('ALTER TABLE obw_product_category DROP FOREIGN KEY FK_C6E2EE0F16FE72E1');
        $this->addSql('DROP INDEX IDX_C6E2EE0FDE12AB56 ON obw_product_category');
        $this->addSql('DROP INDEX IDX_C6E2EE0F16FE72E1 ON obw_product_category');
        $this->addSql('ALTER TABLE obw_product_category DROP created_by, DROP updated_by, DROP created_at, DROP updated_at, DROP deleted_at');
        $this->addSql('ALTER TABLE obw_sport_event DROP FOREIGN KEY FK_C1EBABEEDE12AB56');
        $this->addSql('ALTER TABLE obw_sport_event DROP FOREIGN KEY FK_C1EBABEE16FE72E1');
        $this->addSql('DROP INDEX IDX_C1EBABEEDE12AB56 ON obw_sport_event');
        $this->addSql('DROP INDEX IDX_C1EBABEE16FE72E1 ON obw_sport_event');
        $this->addSql('ALTER TABLE obw_sport_event DROP created_by, DROP updated_by, DROP created_at, DROP updated_at, DROP deleted_at');
        $this->addSql('ALTER TABLE star_batch DROP FOREIGN KEY FK_F7FCBAF8DE12AB56');
        $this->addSql('ALTER TABLE star_batch DROP FOREIGN KEY FK_F7FCBAF816FE72E1');
        $this->addSql('DROP INDEX IDX_F7FCBAF8DE12AB56 ON star_batch');
        $this->addSql('DROP INDEX IDX_F7FCBAF816FE72E1 ON star_batch');
        $this->addSql('ALTER TABLE star_batch DROP created_by, DROP updated_by, DROP created_at, DROP updated_at, DROP deleted_at');
        $this->addSql('ALTER TABLE star_house DROP FOREIGN KEY FK_6822D1B1DE12AB56');
        $this->addSql('ALTER TABLE star_house DROP FOREIGN KEY FK_6822D1B116FE72E1');
        $this->addSql('DROP INDEX IDX_6822D1B1DE12AB56 ON star_house');
        $this->addSql('DROP INDEX IDX_6822D1B116FE72E1 ON star_house');
        $this->addSql('ALTER TABLE star_house DROP created_by, DROP updated_by, DROP created_at, DROP updated_at, DROP deleted_at');
        $this->addSql('ALTER TABLE star_student DROP FOREIGN KEY FK_20C3A120DE12AB56');
        $this->addSql('ALTER TABLE star_student DROP FOREIGN KEY FK_20C3A12016FE72E1');
        $this->addSql('DROP INDEX IDX_20C3A120DE12AB56 ON star_student');
        $this->addSql('DROP INDEX IDX_20C3A12016FE72E1 ON star_student');
        $this->addSql('ALTER TABLE star_student DROP created_by, DROP updated_by, DROP created_at, DROP updated_at, DROP deleted_at');
    }
}
