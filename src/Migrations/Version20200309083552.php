<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200309083552 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE obw_pre_registration_order_status_log (id INT AUTO_INCREMENT NOT NULL, obw_pre_registration_order_id INT DEFAULT NULL, created_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', updated_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', new_status VARCHAR(255) NOT NULL, old_status VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_F0B9AF0D239EFA76 (obw_pre_registration_order_id), INDEX IDX_F0B9AF0DDE12AB56 (created_by), INDEX IDX_F0B9AF0D16FE72E1 (updated_by), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE obw_pre_registration_order_status_log ADD CONSTRAINT FK_F0B9AF0D239EFA76 FOREIGN KEY (obw_pre_registration_order_id) REFERENCES obw_pre_registration_order (id)');
        $this->addSql('ALTER TABLE obw_pre_registration_order_status_log ADD CONSTRAINT FK_F0B9AF0DDE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE obw_pre_registration_order_status_log ADD CONSTRAINT FK_F0B9AF0D16FE72E1 FOREIGN KEY (updated_by) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE obw_pre_registration_order_status_log');
    }
}
