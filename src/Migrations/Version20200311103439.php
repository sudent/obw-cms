<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\StarStudent;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200311103439 extends AbstractMigration implements ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs

        $em = $this->container->get('doctrine.orm.entity_manager');

        $students = $em->getRepository(StarStudent::class)->findAll();

        $existingStudents = [];

        $count = 0;

        /** @var StarStudent $student */
        foreach ($students as $student) {
            $flatData = [strtolower($student->getName()), $student->getHouse()->getId(), $student->getBatch()->getId(), strtolower($student->getContactNumber())];

            $isExist = false;

            foreach ($existingStudents as $existingId => $existingStudent) {
                if ($flatData === $existingStudent['flat_data']) {
                    $existingObwPreRegistrations = $student->getObwPreRegistrations();

                    foreach ($existingObwPreRegistrations as $obwPreRegistration) {
                        $obwPreRegistration->setStudent($existingStudent['obj']);

                        $em->persist($obwPreRegistration);
                    }

                    $em->remove($student);
                    $count++;
                    $isExist = true;

                    break;
                }
            }

            if (!$isExist) {
                $existingStudents[$student->getId()] = [
                    'obj' => $student,
                    'flat_data' => $flatData
                ];
            }
        }

        $em->flush();

        $this->write('Removed ' . $count . ' duplicate students');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
