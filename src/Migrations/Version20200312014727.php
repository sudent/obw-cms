<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Settings\ObwPreRegistrationSetting;
use App\Migrations\AbstractSettingMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200312014727 extends AbstractSettingMigration
{
    public function getSettings() : array
    {
        return [
            ObwPreRegistrationSetting::SECTION => [
                ObwPreRegistrationSetting::SUPER_SENIOR_AFTER_YEARS
            ]
        ];
    }

    public function getDescription() : string
    {
        return '';
    }
}
