<?php

namespace App\Repository;

use App\Entity\ObwContribution;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObwContribution|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObwContribution|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObwContribution[]    findAll()
 * @method ObwContribution[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObwContributionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObwContribution::class);
    }

    // /**
    //  * @return ObwContribution[] Returns an array of ObwContribution objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObwContribution
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
