<?php

namespace App\Repository;

use App\Entity\ObwContributions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObwContributions|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObwContributions|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObwContributions[]    findAll()
 * @method ObwContributions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObwContributionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObwContributions::class);
    }

    // /**
    //  * @return ObwContributions[] Returns an array of ObwContributions objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObwContributions
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
