<?php

namespace App\Repository;

use App\Entity\ObwDinnerTableReservation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObwDinnerTableReservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObwDinnerTableReservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObwDinnerTableReservation[]    findAll()
 * @method ObwDinnerTableReservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObwDinnerTableReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObwDinnerTableReservation::class);
    }

    // /**
    //  * @return ObwDinnerTableReservation[] Returns an array of ObwDinnerTableReservation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObwDinnerTableReservation
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
