<?php

namespace App\Repository;

use App\Entity\ObwMerchandise;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObwMerchandise|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObwMerchandise|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObwMerchandise[]    findAll()
 * @method ObwMerchandise[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObwMerchandiseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObwMerchandise::class);
    }

    // /**
    //  * @return ObwMerchandise[] Returns an array of ObwMerchandise objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObwMerchandise
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
