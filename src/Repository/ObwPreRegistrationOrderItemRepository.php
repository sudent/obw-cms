<?php

namespace App\Repository;

use App\Entity\ObwPreRegistrationOrderItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObwPreRegistrationOrderItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObwPreRegistrationOrderItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObwPreRegistrationOrderItem[]    findAll()
 * @method ObwPreRegistrationOrderItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObwPreRegistrationOrderItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObwPreRegistrationOrderItem::class);
    }

    // /**
    //  * @return ObwPreRegistrationOrderItem[] Returns an array of ObwPreRegistrationOrderItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObwPreRegistrationOrderItem
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
