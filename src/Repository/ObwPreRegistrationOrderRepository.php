<?php

namespace App\Repository;

use App\Entity\ObwPreRegistrationOrder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObwPreRegistrationOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObwPreRegistrationOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObwPreRegistrationOrder[]    findAll()
 * @method ObwPreRegistrationOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObwPreRegistrationOrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObwPreRegistrationOrder::class);
    }

    // /**
    //  * @return ObwPreRegistrationOrder[] Returns an array of ObwPreRegistrationOrder objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObwPreRegistrationOrder
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
