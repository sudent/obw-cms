<?php

namespace App\Repository;

use App\Entity\ObwPreRegistrationOrderStatusLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObwPreRegistrationOrderStatusLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObwPreRegistrationOrderStatusLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObwPreRegistrationOrderStatusLog[]    findAll()
 * @method ObwPreRegistrationOrderStatusLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObwPreRegistrationOrderStatusLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObwPreRegistrationOrderStatusLog::class);
    }

    // /**
    //  * @return ObwPreRegistrationOrderStatusLog[] Returns an array of ObwPreRegistrationOrderStatusLog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObwPreRegistrationOrderStatusLog
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
