<?php

namespace App\Repository;

use App\Entity\ObwProductCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObwProductCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObwProductCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObwProductCategory[]    findAll()
 * @method ObwProductCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObwProductCategoryRepository extends ServiceEntityRepository
{
    private $alias = 'opc';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObwProductCategory::class);
    }

    public function countTotalByName($value)
    {
        $countQB = $this->createQueryBuilder($this->alias);
        $countQB
            ->select($countQB->expr()->count($this->alias))
            ->where($this->alias . '.name LIKE :value')
            ->setParameter('value', '%' . $value . '%')
        ;

        return $countQB->getQuery()->getSingleScalarResult();
    }

    public function findByName($value, $offset, $maxResults = 20)
    {
        $qb = $this->createQueryBuilder($this->alias);

        $qb->where($this->alias . '.name LIKE :value')
            ->setParameter('value', '%' . $value . '%')
            ->orderBy($this->alias . '.name', 'ASC')
            ->setMaxResults($maxResults)
            ->setFirstResult($offset)
        ;

        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return ObwProductCategory[] Returns an array of ObwProductCategory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObwProductCategory
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
