<?php

namespace App\Repository;

use App\Entity\ObwProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObwProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObwProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObwProduct[]    findAll()
 * @method ObwProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObwProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObwProduct::class);
    }

    // /**
    //  * @return ObwProduct[] Returns an array of ObwProduct objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObwProduct
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
