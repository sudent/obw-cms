<?php

namespace App\Repository;

use App\Entity\ObwSportEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObwSportEvent|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObwSportEvent|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObwSportEvent[]    findAll()
 * @method ObwSportEvent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObwSportEventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObwSportEvent::class);
    }

    // /**
    //  * @return ObwSportEvent[] Returns an array of ObwSportEvent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObwSportEvent
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
