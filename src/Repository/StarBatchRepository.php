<?php

namespace App\Repository;

use App\Entity\StarBatch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method StarBatch|null find($id, $lockMode = null, $lockVersion = null)
 * @method StarBatch|null findOneBy(array $criteria, array $orderBy = null)
 * @method StarBatch[]    findAll()
 * @method StarBatch[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StarBatchRepository extends ServiceEntityRepository
{
    private $alias = 'sb';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StarBatch::class);
    }

    public function countTotalByName($value)
    {
        $countQB = $this->createQueryBuilder($this->alias);
        $countQB
            ->select($countQB->expr()->count($this->alias))
            ->where($this->alias . '.name LIKE :value')
            ->setParameter('value', '%' . $value . '%')
        ;

        return $countQB->getQuery()->getSingleScalarResult();
    }

    public function findByName($value, $offset, $maxResults = 20)
    {
        $qb = $this->createQueryBuilder($this->alias);

        $qb->where($this->alias . '.name LIKE :value')
            ->setParameter('value', '%' . $value . '%')
            ->orderBy($this->alias . '.name', 'ASC')
            ->setMaxResults($maxResults)
            ->setFirstResult($offset)
        ;

        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return StarBatch[] Returns an array of StarBatch objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StarBatch
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
