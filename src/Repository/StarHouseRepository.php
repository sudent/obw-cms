<?php

namespace App\Repository;

use App\Entity\StarHouse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method StarHouse|null find($id, $lockMode = null, $lockVersion = null)
 * @method StarHouse|null findOneBy(array $criteria, array $orderBy = null)
 * @method StarHouse[]    findAll()
 * @method StarHouse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StarHouseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StarHouse::class);
    }

    // /**
    //  * @return StarHouse[] Returns an array of StarHouse objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StarHouse
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
