<?php

namespace App\Repository;

use App\Entity\StarStudent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method StarStudent|null find($id, $lockMode = null, $lockVersion = null)
 * @method StarStudent|null findOneBy(array $criteria, array $orderBy = null)
 * @method StarStudent[]    findAll()
 * @method StarStudent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StarStudentRepository extends ServiceEntityRepository
{
    private $alias = 's';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StarStudent::class);
    }

    public function getExistingStudent(StarStudent $student)
    {
        $qb = $this->createQueryBuilder($this->alias);

        $qb->where($this->alias . '.name like :name')
            ->andWhere($this->alias . '.batch = :batch')
            ->andWhere($this->alias . '.house = :house')
            ->andWhere($this->alias . '.contactNumber like :contact_number')
            ->setParameter('name', $student->getName())
            ->setParameter('batch', $student->getBatch())
            ->setParameter('house', $student->getHouse())
            ->setParameter('contact_number', $student->getContactNumber())
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    // /**
    //  * @return StarStudent[] Returns an array of StarStudent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StarStudent
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
