<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use function Doctrine\ORM\QueryBuilder;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findByRoles($role)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->where('u.roles LIKE :roles')
            ->setParameter('roles', '%'.$role.'%');

        return $qb->getQuery()->getResult();
    }

    public function notifyRolesOnNewTicket()
    {
        $qb = $this->createQueryBuilder('u');
        $qb->where('u.roles LIKE :admin_role')
            ->orWhere('u.roles LIKE :technician_role')
            ->orWhere('u.roles LIKE :super_admin_role')
            ->setParameter('admin_role', '%'. User::ROLE_ADMIN .'%')
            ->setParameter('technician_role', '%'. User::ROLE_TECHNICIAN .'%')
            ->setParameter('super_admin_role', '%'. User::ROLE_SUPER_ADMIN .'%');

        return $qb->getQuery()->getResult();
    }

    public function countTotalByTechnicianRoleAndName($value)
    {
        $countQB = $this->createQueryBuilder('u');
        $countQB
            ->select($countQB->expr()->count('u'))
            ->join('u.profile', 'p')
            ->where($countQB->expr()->orX(
                $countQB->expr()->like('u.roles', ':roleTechnician'),
                $countQB->expr()->like('u.roles', ':roleAdmin'),
                $countQB->expr()->like('u.roles', ':roleSuperAdmin')
            ))
            ->andWhere(
                $countQB->expr()->orX(
                    $countQB->expr()->like('u.username', ':username'),
                    $countQB->expr()->like('p.fullName', ':fullName')
                )
            )
            ->setParameter('username', '%' . $value . '%')
            ->setParameter('fullName', '%' . $value . '%')
            ->setParameter('roleTechnician', '%' . User::ROLE_TECHNICIAN . '%')
            ->setParameter('roleAdmin', '%' . User::ROLE_ADMIN . '%')
            ->setParameter('roleSuperAdmin', '%' . User::ROLE_SUPER_ADMIN . '%')
        ;

        return $countQB->getQuery()->getSingleScalarResult();
    }

    public function findByTechnicianRoleAndName($value, $offset, $maxResults = 20)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->join('u.profile', 'p');

        $qb->where($qb->expr()->orX(
                $qb->expr()->like('u.roles', ':roleTechnician'),
                $qb->expr()->like('u.roles', ':roleAdmin'),
                $qb->expr()->like('u.roles', ':roleSuperAdmin')
            ))
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like('u.username', ':username'),
                    $qb->expr()->like('p.fullName', ':fullName')
                )
            )
            ->setParameter('username', '%' . $value . '%')
            ->setParameter('fullName', '%' . $value . '%')
            ->setParameter('roleTechnician', '%' . User::ROLE_TECHNICIAN . '%')
            ->setParameter('roleAdmin', '%' . User::ROLE_ADMIN . '%')
            ->setParameter('roleSuperAdmin', '%' . User::ROLE_SUPER_ADMIN . '%')
            ->setMaxResults($maxResults)
            ->setFirstResult($offset)
            ;

        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
