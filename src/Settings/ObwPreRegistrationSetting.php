<?php

namespace App\Settings;

class ObwPreRegistrationSetting
{
    const SECTION = 'obw_pre_registration';

    const PRODUCT_JERSEY_ID = [
        'name' => 'product_jersey_id',
        'type' => 'entity',
        'class' => 'App\\Entity\\ObwProduct',
        'label' => 'Set Jersey Product',
        'placeholder' => 'Select Product'
    ];

    const PRODUCT_DINNER_TABLE_ID = [
        'name' => 'product_dinner_table_id',
        'type' => 'entity',
        'class' => 'App\\Entity\\ObwProduct',
        'label' => 'Set Dinner Table Product',
        'placeholder' => 'Select Product'
    ];

    const PRODUCT_PRE_REGISTRATION_ID = [
        'name' => 'product_pre_registration_id',
        'type' => 'entity',
        'class' => 'App\\Entity\\ObwProduct',
        'label' => 'Set Pre-Registration Product',
        'placeholder' => 'Select Product'
    ];

    const PRODUCT_FUND_CONTRIBUTION = [
        'name' => 'product_fund_contribution',
        'type' => 'entity',
        'class' => 'App\\Entity\\ObwProduct',
        'label' => 'Set OBW Fund Contribution Product',
        'placeholder' => 'Select Product'
    ];

    const CONTRIBUTION_CATEGORY = [
        'name' => 'contribution_category',
        'type' => 'entity',
        'class' => 'App\\Entity\\ObwProductCategory',
        'label' => 'Set Which Category For Contribution Products',
        'placeholder' => 'Select Category'
    ];

    const OBW_DATE = [
        'name' => 'obw_date',
        'type' => 'date',
        'label' => 'Set Expected OBW Date'
    ];

    const SUPER_SENIOR_AFTER_YEARS = [
        'name' => 'super_senior_after_years',
        'type' => 'number',
        'label' => 'After how many years previously from now is considered super senior?'
    ];

    static function getDefs($name)
    {
        $reflectionClass = new \ReflectionClass(__CLASS__);

        foreach ($reflectionClass->getConstants() as $constant => $values) {
            if ($constant == 'SECTION') {
                continue;
            } elseif ($values['name'] == $name) {
                return $values;
            }
        }

        return false;
    }
}
