<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\TwigTest;

class AppExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return array(
            new TwigFunction('instanceof', array(AppRuntime::class, 'isInstanceof')),
            new TwigFunction('is_granted_user', array(AppRuntime::class, 'isGrantedUser')),
            new TwigFunction('ucwords', 'ucwords'),
        );
    }

    public function getTests()
    {
        return array(
            new TwigTest('instanceof', array(AppRuntime::class, 'isInstanceof')),
        );
    }

    public function getFilters()
    {
        return array(
            new TwigFilter('ucwords', 'ucwords'),
            new TwigFilter('format_price', array(AppRuntime::class, 'formatPrice')),
        );
    }
}
