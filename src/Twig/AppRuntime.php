<?php

namespace App\Twig;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Twig\Extension\RuntimeExtensionInterface;

class AppRuntime implements RuntimeExtensionInterface
{
    private $urlGenerator;
    private $requestStack;
    private $tokenStorage;
    private $authenticationManager;
    private $accessDecisionManager;

    public function __construct(
        RequestStack $requestStack,
        UrlGeneratorInterface $urlGenerator,
        TokenStorageInterface $tokenStorage,
        AuthenticationManagerInterface $authenticationManager,
        AccessDecisionManagerInterface $accessDecisionManager
    ) {
        $this->requestStack = $requestStack;
        $this->urlGenerator = $urlGenerator;
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
        $this->accessDecisionManager = $accessDecisionManager;
    }

    public function isInstanceOf($var, $instance) {
        $reflectionClass = new \ReflectionClass($instance);

        return $reflectionClass->isInstance($var);
    }

    public function isGrantedUser($role, UserInterface $user)
    {
        $token = new UsernamePasswordToken(
            $user,
            null,
            'main',
            $user->getRoles()
        );

        if (!$this->accessDecisionManager->decide($token, array($role))) {
            return false;
        }

        return true;
    }

    public function formatPrice($price, $showCurrency = true)
    {
        return ($showCurrency ? 'RM' : '') . number_format(($price / 100), '2', '.', ',');
    }
}
