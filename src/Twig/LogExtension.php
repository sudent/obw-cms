<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\TwigTest;

class LogExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return array(
            new TwigFunction('entity_logs', array(LogRuntime::class, 'entityLogs'), ['is_safe' => ['html']]),
        );
    }

    public function getTests()
    {
        return array(
            new TwigTest('instanceof', array(AppRuntime::class, 'isInstanceof')),
        );
    }

    public function getFilters()
    {
        return array(
            new TwigFilter('ucwords', 'ucwords'),
        );
    }
}
