<?php

namespace App\Twig;

use App\Entity\Interfaces\LoggableInterface;
use App\Entity\User;
use DataDog\AuditBundle\Entity\Association;
use DataDog\AuditBundle\Entity\AuditLog;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Gedmo\Tool\Wrapper\EntityWrapper;
use Twig\Environment;
use Twig\Error\RuntimeError;
use Twig\Extension\RuntimeExtensionInterface;

class LogRuntime implements RuntimeExtensionInterface
{
    private $entityManager;
    private $twig;

    public function __construct(EntityManagerInterface $entityManager, Environment $twig) {
        $this->entityManager = $entityManager;
        $this->twig = $twig;
    }

    public function entityLogs($entity) {
        if (!$entity instanceof LoggableInterface) {
            throw new RuntimeError('Entity does not implement LoggableInterface');
        }

        $wrapped = new EntityWrapper($entity, $this->entityManager);
        $objectMeta = $wrapped->getMetadata();
        $objectClass = $objectMeta->name;

        $qb = $this->entityManager->getRepository("DataDogAuditBundle:AuditLog")
            ->createQueryBuilder('a')
            ->addSelect('s', 't', 'b')
            ->innerJoin('a.source', 's')
            ->leftJoin('a.target', 't')
            ->leftJoin('a.blame', 'b')
            ->where('s.class = :class')
            ->andWhere('s.fk = :fk')
            ->orderBy('a.loggedAt', 'desc')
            ->setParameter('class', $objectClass)
            ->setParameter('fk', $entity->getId())
        ;

        $logs = $qb->getQuery()->getResult();

        if ($logs) {
            $content = '';

            /** @var AuditLog $log */
            foreach ($logs as $log) {
                $action = $log->getAction();
                $loggedAt = $log->getLoggedAt();

                if ($log->getBlame()) {
                    $user = $this->entityManager->getRepository(User::class)->find($log->getBlame()->getFk());
                    if (!$user) {
                        $user = $log->getBlame()->getLabel();
                    }
                }

                $changes = [];

                if ($log->getDiff()) {
                    foreach ($log->getDiff() as $field => $change) {
                        // Skip this fields when displaying audit log
                        if (in_array($field, ['createdAt', 'updatedAt', 'deletedAt', 'id']))
                            continue;

                        $this->mapValue($objectMeta, $field, $change['new']);

                        $changes[] = [
                            'label' => $objectClass::getLabelForField($field),
                            'data' => $this->value($change['new'], $field)
                        ];
                    }
                } elseif ($action == 'associate' || $action == 'dissociate') {
                    $source = $entity;
                    /** @var Association $target */
                    $target = $log->getTarget();
                    $targetEntity = $this->entityManager->getRepository($target->getClass())->find($target->getFk());

                    if (!$targetEntity) {
                        $changes = [
                            'targetLabel' => $objectClass::getLabelForAssociation($target->getClass()),
                            'target' => $target->getLabel()
                        ];
                    } else {
                        $changes = [
                            'targetLabel' => $objectClass::getLabelForAssociation($targetEntity),
                            'target' => $targetEntity
                        ];
                    }
                }

                $content .= $this->twig->render("log/{$action}.html.twig", compact('action', 'user', 'changes', 'loggedAt'));
            }

            return $content;
        }

        return 'No log found for ' . $entity;
    }

    /**
     * @param ClassMetadata $objectMeta
     * @param string        $field
     * @param mixed         $value
     */
    protected function mapValue(ClassMetadata $objectMeta, $field, &$value)
    {
        if (!$objectMeta->isSingleValuedAssociation($field)) {
            return;
        }

        $mapping = $objectMeta->getAssociationMapping($field);
        $value   = $value ? $this->entityManager->getReference($mapping['targetEntity'], $value['fk']) : null;
    }

    protected function value($val, $field = null)
    {
        switch (true) {
            case is_bool($val):
                return $val ? 'Yes' : 'No';
            case is_array($val):
                return json_encode($val);
            case is_null($val):
                return 'NULL';
            case is_object($val):
                if ($val instanceof \DateTime) {
                    if ($field == 'procurementYear') {
                        return $val->format('Y');
                    }

                    return $val->format('Y-m-d h:i:s a');
                } elseif (method_exists($val, '__toString' )) {
                    return $val->__toString();
                } else {
                    return $val;
                }
            default:
                if ($field == 'procurementYear') {
                    $val = new \DateTime($val);
                    return $val->format('Y');
                }
                return $val;
        }
    }

}
