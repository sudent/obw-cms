<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class FileNotEmptyValidator extends ConstraintValidator
{
    /**
     * @param Constraint $constraint
     */
    public function validate($file, Constraint $constraint)
    {
        /* @var $constraint FileNotEmpty */

        if (null === $file || '' === $file) {
            return;
        }

        if (null === $file->getId() && (null === $file->getUploadedFile() || '' === $file->getUploadedFile())) {
            // TODO: implement the validation here
            $this->context->buildViolation($constraint->message)
                ->setTranslationDomain('validators')
                ->addViolation();
        }
    }
}
