<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class InviteCodeValid extends Constraint
{
    public $inviteCodeUsed = 'Invite code already have been used';
    public $invalidInviteCode = 'Invite code is invalid';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
