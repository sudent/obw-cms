<?php

namespace App\Validator;

use App\Entity\InviteCode;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class InviteCodeValidValidator extends ConstraintValidator
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof InviteCodeValid) {
            throw new UnexpectedTypeException($constraint, InviteCodeValid::class);
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }

        $inviteCode = $this->entityManager->getRepository(InviteCode::class)->findOneBy([
            'code' => $value
        ]);

        if (!$inviteCode) {
            $this->context->buildViolation($constraint->invalidInviteCode)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        } elseif ($inviteCode && $inviteCode->getIsAlreadyUsed()) {
            $this->context->buildViolation($constraint->inviteCodeUsed)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }
}
